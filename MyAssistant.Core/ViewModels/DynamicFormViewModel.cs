﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Autofac;
using MyAssistant.Management;
using MyAssistant.Model;
using MyAssistant.Plugin;
using MyAssistant.Services;
using ReactiveUI;
using Splat;

namespace MyAssistant.ViewModels
{
    public class DynamicFormViewModel : ReactiveObject
    {
        /// <summary>
        ///     数据库清单
        /// </summary>
        private readonly ObservableAsPropertyHelper<List<string>> _databaseList;


        private readonly IDatabaseService _databaseService;
        private readonly IComponentContext _componentContext;
        private readonly IDirectoryIISService _iisService;

        private readonly IInspectService _inspectService;
        private readonly ILibraryService _libraryService;


        /// <summary>
        ///     必填验证情况
        /// </summary>
        private bool _isValid;



        public DynamicFormViewModel(SiteInfo site,
            IDatabaseService databaseService,
            IComponentContext componentContext,
            IDirectoryIISService iisService,
            IInspectService inspectService,
            ILibraryService libraryService)
        {
            _databaseService = databaseService;
            _componentContext = componentContext;
            _iisService = iisService;
            _inspectService = inspectService;
            _libraryService = libraryService;

            Site = site;

            //控制添加内网地址按钮
            var siteMode = libraryService.Get().Mode;
            SetAddVisibility(siteMode);

              //主数据源
              Data = Site.Data.CreateDerivedCollection(x => x);
            Data.ChangeTrackingEnabled = true;
            Data.ItemChanged.Select(p => CheckFormValid()).Subscribe(p => IsValid = p);

            //内网地址移除
            RemoveLoadBalanceFieldCommand = ReactiveCommand.Create<SiteAttribute, Unit>
            (p =>
            {
                Site.Data.Remove(p);
                return Unit.Default;
            });

            //添加内网地址
            AddLoadBalanceFieldCommand = ReactiveCommand.Create
                (() => { Site.Data.Add(new SiteAttribute {Name = "内网地址", Value = "http://", Display = true}); });

            if (CheckFormValid())
                IsValid = true;

            GetPluginCommand = ReactiveCommand.CreateFromTask<bool, List<IPluginService>>(async (b, token) => await GetPlugins());

            ExecuteSearch =
                ReactiveCommand.CreateFromTask<DatabaseInfo, List<string>>(
                    info => { return Task.Run(() => _databaseService.GetAllDatabase(info)); });



            SetModeCommand = ReactiveCommand.CreateFromTask<EnumMode, Unit>(async (mode, token) => await SetMode(mode));

            SetDataChangeEvent();


            var dbColumn = site.Data.FirstOrDefault(p => p.Name == "数据库名");

            if (dbColumn != null && dbColumn.Display.Value)
                ExecuteSearch.ToProperty(this, x => x.DatabaseList, out _databaseList,
                    _databaseService.GetAllDatabase(GetDatabaseInfo()));

            var sitePathColumn = site.Data.FirstOrDefault(p => p.Name == "站点位置");

            if (sitePathColumn != null)
                IISSites = iisService.GetALLSites();

        }


        private void SetAddVisibility(EnumMode mode)
        {
            if (mode == EnumMode.LoadBalance)
                HideAdd = false;
        }

        /// <summary>
        /// 设置数据变化事件
        /// </summary>
        private void SetDataChangeEvent()
        {
            Func<DatabaseInfo, bool> checkDbReq = o => (string.IsNullOrEmpty(o.Server) == false)
                                                   && (string.IsNullOrEmpty(o.Password) == false)
                                                   && (string.IsNullOrEmpty(o.User) == false);


            Data.ItemChanged.Throttle(TimeSpan.FromMilliseconds(500), RxApp.MainThreadScheduler)
                .Where(p => "DB服务器名 密码 用户名".Split(' ').Contains(p.Sender.Name))
                .Subscribe(a =>
                {
                    var b = Site.Data.FirstOrDefault(p => p.Name == "数据库名");
                    if (b != null)
                        b.Value = "";
                });


            Data.ItemChanged.Throttle(TimeSpan.FromMilliseconds(500), RxApp.MainThreadScheduler)
                .Where(p => "DB服务器名 密码 用户名".Split(' ').Contains(p.Sender.Name))
                .Select(p => GetDatabaseInfo())
                .DistinctUntilChanged()
                .Where(checkDbReq)
                .InvokeCommand(ExecuteSearch);


            Data.ItemChanged.Throttle(TimeSpan.FromMilliseconds(500), RxApp.MainThreadScheduler)
                .Where(p => p.Sender.Name == "站点位置")
                .Subscribe(o =>
                {
                    var val = o.Sender.Value;

                    var iisSite = _iisService.GetALLSites()?.FirstOrDefault(p => p.SiteName == val);

                    if (iisSite != null)
                    {
                        var info = _iisService.GetErpInfo(val, Site.Type == "重构");
                        if (info != null)
                        {
                            var dbFiled = Site.Data.FirstOrDefault(p => p.Name == "数据库名");
                            if (dbFiled != null)
                                dbFiled.Value = info.DBName;
                            var serverField = Site.Data.FirstOrDefault(p => p.Name == "DB服务器名");
                            if (serverField != null)
                                serverField.Value = info.DBAddress;
                            var userField = Site.Data.FirstOrDefault(p => p.Name == "用户名");
                            if (userField != null)
                                userField.Value = info.DBUser;
                            var pwdFeild = Site.Data.FirstOrDefault(p => p.Name == "密码");
                            if (pwdFeild != null)
                                pwdFeild.Value = info.DBPwd;
                            var siteUrlField = Site.Data.FirstOrDefault(p => p.Name == "站点地址");
                            if (siteUrlField != null)
                                siteUrlField.Value = info.ERPSite;
                        }
                    }
                });
        }

        /// <summary>
        ///     初始化命令
        /// </summary>
        public ReactiveCommand<bool, List<IPluginService>> GetPluginCommand { get; set; }

        private bool _hideAdd = true;
        public bool HideAdd
        {
            get { return _hideAdd; }
            set { this.RaiseAndSetIfChanged(ref _hideAdd, value); }
        }

        /// <summary>
        ///     站点信息
        /// </summary>
        public SiteInfo Site { get; set; }

        /// <summary>
        ///     表单数据
        /// </summary>
        public IReactiveDerivedList<SiteAttribute> Data { get; set; }

        public List<string> DatabaseList => _databaseList.Value;


        public List<IISSite> IISSites { get; set; }


        public ReactiveCommand<SiteAttribute, Unit> RemoveLoadBalanceFieldCommand { get; set; }

        public ReactiveCommand<Unit, Unit> AddLoadBalanceFieldCommand { get; set; }

        public ReactiveCommand<EnumMode, Unit> SetModeCommand { get; set; }


        /// <summary>
        ///     数据库查找命令
        /// </summary>
        public ReactiveCommand<DatabaseInfo, List<string>> ExecuteSearch { get; protected set; }

        public bool IsValid
        {
            get { return _isValid; }
            set { this.RaiseAndSetIfChanged(ref _isValid, value); }
        }


        /// <summary>
        ///     获取数据库连接
        /// </summary>
        /// <returns></returns>
        private DatabaseInfo GetDatabaseInfo()
        {
            return new DatabaseInfo
            {
                Server = Data.FirstOrDefault(q => q.Name == "DB服务器名")?.Value,
                Password = Data.FirstOrDefault(q => q.Name == "密码")?.Value,
                User = Data.FirstOrDefault(q => q.Name == "用户名")?.Value
            };
        }

        /// <summary>
        ///     页面必填检查
        /// </summary>
        /// <returns></returns>
        private bool CheckFormValid()
        {
            if (Site.Required == false)
                return true;

            if (Data.FirstOrDefault(v => v.Display.Value && string.IsNullOrEmpty(v.Value)) != null)
                return false;
            return true;
        }

        /// <summary>
        ///     子站点初始化
        /// </summary>
        /// <returns></returns>
        private Task<List<IPluginService>> GetPlugins()
        {
            return Task.Run(() =>
            {
                try
                {
                    var inspects = _inspectService.GetSiteInspect(Site);

                    var pluginServices = new List<IPluginService>();

                    foreach (var inspect in inspects)
                    {
                        var _pluginService =
                            new PluginService(PluginUtilities.Resolve(_componentContext, Site.BusinessClass));

                        _pluginService.SetType(inspect.Url, Site.Title, _libraryService.Get());

                        pluginServices.Add(_pluginService);
                    }

                    return pluginServices;
                }
                catch (Exception e)
                {
                    this.Log().ErrorException(Site.Title, e);
                    return null;
                }
            });
        }

        private readonly Dictionary<EnumMode, string[]> Dict = new Dictionary<EnumMode, string[]>
        {
            {EnumMode.SignleServer, new[] {"站点位置", "站点地址", "DB服务器名", "用户名", "密码", "数据库名"}},
            {EnumMode.LoadBalance, new[] {"内网地址", "站点地址", "DB服务器名", "用户名", "密码", "数据库名"}},
            {EnumMode.MultiServer, new[] {"站点地址", "DB服务器名", "用户名", "密码", "数据库名"}}
        };

        private Task<Unit> SetMode(EnumMode mode)
        {

           return  Task.Run(() =>
            {
                var list = Dict.GetValue(mode);
                SetAddVisibility(mode);
                foreach (var row in Site.Data)
                {
                    if (list.Contains(row.Name))
                        row.Display = true;
                    else
                    {
                        row.Display = false;
                        row.Value = row.Name == "内网地址" ? "http://" : "";
                    }
                }
                return Unit.Default;
            });
        }

    }
}