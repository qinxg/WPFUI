﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Text;
using System.Threading.Tasks;
using MyAssistant.Model;
using MyAssistant.Services;
using ReactiveUI;
using Splat;

// ReSharper disable once CheckNamespace
namespace MyAssistant.ViewModels
{
    /// <summary>
    /// 设置视图
    /// </summary>
    public class SettingsViewModel : ReactiveObject
    {

        /// <summary>
        /// 站点信息
        /// </summary>
        public List<SiteInfo> Sites { get; set; }

        public SettingsViewModel(ILibraryService service)
        {
            Sites = service.Get().Sites;
        }
    }
}
