﻿这里的命名空间做了一些处理。
具体原因看http://caliburnmicro.com/documentation/naming-conventions
如果包含其他内容，无法正确识别


 	View						Model											View
Convention	<RootNS>.ViewModels.<ChildNS>.<ViewModelTypeName>	<RootNS>.Views.<ChildNS>.<ViewTypeName>
Example 1	MyProject.ViewModels.ShellViewModel	MyProject.Views.ShellView
Example 2	MyProject.ViewModels.Utilities.SettingsViewModel	MyPoject.Views.Utitlities.SettingsView
