﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using System.Threading.Tasks;
using Autofac;
using MahApps.Metro.Controls.Dialogs;
using MyAssistant.Management;
using MyAssistant.Model;
using MyAssistant.Services;
using MyAssistant.Utility;
using ReactiveUI;

namespace MyAssistant.ViewModels
{
    public class InitViewModel : ReactiveObject
    {
        private readonly IDatabaseService _databaseService;


        /// <summary>
        ///     是否在执行
        /// </summary>
        private readonly ObservableAsPropertyHelper<bool> _isExecuting;


        private readonly ILibraryService _libraryService;


        /// <summary>
        ///     是否可以初始化
        /// </summary>
        private bool _canInit;


        /// <summary>
        ///     是否已经初始化了
        /// </summary>
        private bool _initComplete;

        /// <summary>
        ///     最后初始化时间
        /// </summary>
        private DateTime? _lastInitDateTime;


        /// <summary>
        ///     最后初始化时间文本
        /// </summary>
        private string _lastInitTimeStr;


        private Library _oldLibrary;


        private EnumMode _siteMode;

        public InitViewModel(
            ILibraryService libraryService,
            IComponentContext componentContext,
            IDatabaseService databaseService)
        {
            _libraryService = libraryService;
            _databaseService = databaseService;

            SiteInfoList = new ReactiveList<SiteInfo>();

            DynamicForms =
                SiteInfoList.CreateDerivedCollection(
                    site => componentContext.Resolve<DynamicFormViewModel>(new NamedParameter("site", site)));

            DynamicForms.ChangeTrackingEnabled = true;

            DynamicForms.ItemChanged.Select(x =>
                    DynamicForms.Any(p => !p.IsValid)).Subscribe(p => CanInit = !p);

            InitCommand = ReactiveCommand.CreateFromTask<bool, bool>(async (b, token) => await Init(),
                this.WhenAnyValue(x => x.CanInit));

            InitCommand.Subscribe(b =>
            {
                if (b)
                    SetInitTime();
                else
                    LastInitTimeStr = "初始化失败，请根据提示修改后重新初始化。";
            });

            _isExecuting = InitCommand.IsExecuting.ToProperty(this, x => x.IsExecuting, out _isExecuting);


            ChangeModeCommand =
                ReactiveCommand.CreateFromTask<string, bool>(async (mode, token) => await ChangeMode(mode));

            InitUI();
        }

        /// <summary>
        ///     站点信息集合
        /// </summary>
        private ReactiveList<SiteInfo> SiteInfoList { get; }

        /// <summary>
        ///     页面表单
        /// </summary>
        public IReactiveDerivedList<DynamicFormViewModel> DynamicForms { get; set; }

        /// <summary>
        ///     初始化按钮命令
        /// </summary>
        public ReactiveCommand<bool, bool> InitCommand { get; set; }


        public ReactiveCommand<string, bool> ChangeModeCommand { get; set; }

        public bool IsExecuting => _isExecuting.Value;

        public bool InitComplete
        {
            get { return _initComplete; }
            set { this.RaiseAndSetIfChanged(ref _initComplete, value); }
        }

        public bool CanInit
        {
            get { return _canInit; }
            set { this.RaiseAndSetIfChanged(ref _canInit, value); }
        }

        public EnumMode SiteMode
        {
            get { return _siteMode; }
            set { this.RaiseAndSetIfChanged(ref _siteMode, value); }
        }

        public DateTime? LastInitDateTime
        {
            get { return _lastInitDateTime; }
            set { this.RaiseAndSetIfChanged(ref _lastInitDateTime, value); }
        }

        public string LastInitTimeStr
        {
            get { return _lastInitTimeStr; }
            set { this.RaiseAndSetIfChanged(ref _lastInitTimeStr, value); }
        }


        private Task<bool> ChangeMode(string mode)
        {
            return Task.Run(async () =>
            {
                SiteMode = (EnumMode) Convert.ToInt32(mode);

                //刷新其他页面

                foreach (var form in DynamicForms.OrderBy(p => p.Site.Order))
                {
                    await form.SetModeCommand.Execute(SiteMode);
                }

                return true;
            });
        }


        /// <summary>
        ///     初始化UI
        /// </summary>
        private void InitUI()
        {
            var library = _libraryService.Get();
            SiteInfoList.AddRange(library.Sites);
            InitComplete = library.InitComplete;
            LastInitDateTime = library.LastInitTime;
            _oldLibrary = library.CloneJson();

            SetInitTime();
            SiteMode = library.Mode;

            CanInit = DynamicForms.All(p => p.IsValid);
        }


        private void SetInitTime()
        {
            if (_oldLibrary.InitComplete && _oldLibrary.LastInitTime.HasValue)
                LastInitTimeStr = $"上次初始化时间： {_oldLibrary.LastInitTime.Value:F}";
        }


        private void MessageDialog(string title, string msg)
        {
            var metroDialogSettings = new MetroDialogSettings
            {
                NegativeButtonText = "CANCEL",
                SuppressDefaultResources = true
            };

            DialogCoordinator.Instance.ShowMessageAsync(this, $"{title}", msg, MessageDialogStyle.Affirmative,
                metroDialogSettings);
        }

        public void LoadInitForm()
        {
            SiteInfoList.Clear();
            var sites = _libraryService.GetSelectedSites();
            SiteInfoList.AddRange(sites);
        }

        /// <summary>
        ///     初始化方法
        /// </summary>
        /// <returns></returns>
        private Task<bool> Init()
        {
            return Task.Run(async () =>
            {
                LastInitTimeStr = "收集初始化信息中..";

                var library = _libraryService.Get();

                library.InitComplete = true;

                if (library.InitComplete)
                    library.LastInitTime = DateTime.Now;

                library.Sites = SiteInfoList.ToList();
                library.Mode = SiteMode;
                _libraryService.Set(library);


                var pluginServices = new List<IPluginService>();

                //获取初始化信息
                foreach (var form in DynamicForms.OrderBy(p => p.Site.Order))
                {
                    var result = await form.GetPluginCommand.Execute(true);

                    if (result != null)
                    {
                        pluginServices.AddRange(result);
                        continue;
                    }


                    _libraryService.Set(_oldLibrary);
                    MessageDialog($"{form.Site.Title} 发生错误", "获取初始化信息失败，请检查站点配置");

                    return false;
                }

                if (library.Mode != EnumMode.SignleServer)
                    foreach (var plugin in pluginServices)
                    {
                        var title = plugin.GetTitle();

                        LastInitTimeStr = $"站点{title}初始化检查..";

                        var result = await plugin.CheckConfig();
                        if (result) continue;

                        _libraryService.Set(_oldLibrary);
                        MessageDialog($"{title} 发生错误", "站点地址错误或站点Web.config未正确配置IntegrationModule节点。请检查站点地址并按指引配置。");

                        return false;
                    }


                //执行初始化
                foreach (var plugin in pluginServices)
                {
                    var title = plugin.GetTitle();
                    LastInitTimeStr = $"正在初始化{title}..";

                    var result = await plugin.Init();
                    if (result.Succes) continue;

                    _libraryService.Set(_oldLibrary);
                    MessageDialog($"{title} 发生错误", result.Message);
                }


                //执行数据库脚本
                try
                {
                    LastInitTimeStr = "正在执行初始化脚本..";
                    _databaseService.ExecCommand(library);
                }
                catch (Exception ex)
                {
                    _libraryService.Set(_oldLibrary);
                    MessageDialog("初始化脚本发生错误", ex.Message);
                    return false;
                }

                InitComplete = library.InitComplete;
                LastInitDateTime = library.LastInitTime;
                if (library.Mode != EnumMode.SignleServer)
                {
                    var msg = "当前模式无法自动设置Upfiles站点虚拟目录，需要手动添加站点Upfiles虚拟目录到主站点位置。\r\n" +
                              "请回收所有站点应用程序池，避免站点缓存不一致导致运行出错。";
                    MessageDialog("配置提醒", msg);
                }
                else
                {
                    var msg = "请回收所有站点应用程序池，避免站点缓存不一致导致运行出错。";
                    MessageDialog("配置提醒", msg);
                }


                _oldLibrary = library.CloneJson();
                return true;
            });
        }
    }
}