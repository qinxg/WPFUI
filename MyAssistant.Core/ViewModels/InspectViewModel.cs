﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Security.Policy;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using MyAssistant.Core;
using MyAssistant.Model;
using MyAssistant.Plugin;
using MyAssistant.Services;
using MyAssistant.Utility;
using ReactiveUI;
using Splat;

// ReSharper disable once CheckNamespace

namespace MyAssistant.ViewModels
{
    public class InspectViewModel : ReactiveObject, IEnableLogger
    {
        /// <summary>
        ///     是否在执行
        /// </summary>
        private readonly ObservableAsPropertyHelper<bool> _isExecuting;

        /// <summary>
        ///     最后初始化时间文本
        /// </summary>
        private readonly ObservableAsPropertyHelper<string> _lastInspectTimeStr;


        /// <summary>
        ///     展示检查页面
        /// </summary>
        private readonly ObservableAsPropertyHelper<bool> _showInspect;

        private string _doing;

        private AsyncObservableCollection<InspectionResultViewModel> _inspectionResults;

        /// <summary>
        ///     最后分析化时间
        /// </summary>
        private DateTime? _lastInspectDateTime;


        /// <summary>
        ///     显示默认页面
        /// </summary>
        private bool _showDefault;


        public ILibraryService LibraryService { get; set; }
        public IInspectService InspectService { get; set; }
        public IComponentContext ComponentContext { get; set; }

        public InspectViewModel(ILibraryService libraryService, IComponentContext componentContext,IInspectService inspectService)
        {
            ComponentContext = componentContext;
            InspectService = inspectService;

            LibraryService = libraryService;
            this.WhenAnyValue(x => x.LastInspectDateTime, x => x.HasValue ? $"上次分析时间： {x.Value:F}" : "从未分析过，快开始分析吧！")
                .ToProperty(this, x => x.LastInspectTimestr, out _lastInspectTimeStr);

            this.WhenAnyValue(x => x.ShowDefault, x => !x).ToProperty(this, x => x.ShowInspect, out _showInspect);

            InspectCommond = ReactiveCommand.CreateFromObservable<Unit, long>
                (p => Observable.StartAsync(this.Inspect).TakeUntil(this.CancelCommond));


            InspectCommond.Subscribe(time =>
            {
                var library = LibraryService.Get();
                Doing = "保存最后检查时间...";
                library.LastIspectTime = DateTime.Now;
                LibraryService.Set(library);

                Doing = $"检测完成 ， 耗时{time/1000.00:F2}秒。";
                LastInspectDateTime = library.LastIspectTime;
            });

            _isExecuting = InspectCommond.IsExecuting.ToProperty(this, x => x.IsExecuting, out _isExecuting);

            ShowHelp = ReactiveCommand.CreateFromTask<object, Unit>((o, token) =>
            {

                return Task.Run(() =>
                {
                    var s = (Tuple<string, string>)o;

                    string applicationDirectory = Path.GetDirectoryName(AppInfo.RunPath);
                    if (applicationDirectory != null)
                    {
                        string myFile = Path.Combine(applicationDirectory, "data", "help", $"{s.Item1}.html");

                        if (File.Exists(myFile))
                            Process.Start("iexplore", $"{myFile}#{s.Item2}");
                    }

                    return Unit.Default;
                });
            });


            CancelCommond = ReactiveCommand.Create(() => { });
            CancelCommond.Subscribe(p =>
            {
                this.ShowDefault = true;
            });

            ShowDefault = true;
            InspectionResults = new AsyncObservableCollection<InspectionResultViewModel>();
            LastInspectDateTime = LibraryService.Get().LastIspectTime;
        }


        public bool IsExecuting => _isExecuting.Value;

        public bool ShowDefault
        {
            get { return _showDefault; }
            set { this.RaiseAndSetIfChanged(ref _showDefault, value); }
        }

        public bool ShowInspect => _showInspect.Value;

        public DateTime? LastInspectDateTime
        {
            get { return _lastInspectDateTime; }
            set { this.RaiseAndSetIfChanged(ref _lastInspectDateTime, value); }
        }

        public string Doing
        {
            get { return _doing; }
            set { this.RaiseAndSetIfChanged(ref _doing, value); }
        }

        public string LastInspectTimestr => _lastInspectTimeStr.Value;

        /// <summary>
        ///     分析按钮
        /// </summary>
        public ReactiveCommand<Unit, long> InspectCommond { get; set; }


        public ReactiveCommand<object, Unit> ShowHelp { get; set; }


        /// <summary>
        ///     取消分析按钮
        /// </summary>
        public ReactiveCommand<Unit, Unit> CancelCommond { get; set; }

        public AsyncObservableCollection<InspectionResultViewModel> InspectionResults
        {
            get { return _inspectionResults; }
            set { this.RaiseAndSetIfChanged(ref _inspectionResults, value); }
        }

     


        /// <summary>
        /// 执行检查
        /// </summary>
        /// <param name="ct"></param>
        /// <returns></returns>
        private async Task<long> Inspect(CancellationToken ct)
        {
            InspectionResults.Clear();
            ShowDefault = false;

            Doing = "初始化检测站点清单..";
            await Task.Delay(TimeSpan.FromMilliseconds(100), ct);

            var watch = new Stopwatch();
            watch.Start();

            var library = LibraryService.Get();

            foreach (var site in library.Sites)
            {
                foreach (var inspect in InspectService.GetSiteInspect(site))
                {
                    InspectionResults.Add(inspect);
                }
            }

            foreach (var inspecter in InspectionResults)
            {
                Doing = $"初始化{inspecter.Site.Title}检测清单..";

                //获取站点的所有检测项

                var pluginService =
                    new PluginService(PluginUtilities.Resolve(ComponentContext,
                        inspecter.Site.BusinessClass));

                pluginService.SetType(inspecter.Url, inspecter.Site.Title, library);
                inspecter.Data = pluginService.LoadSiteInspectionItem();

                pluginService.BeforeInspect();

                await Task.Delay(TimeSpan.FromMilliseconds(500), ct);

                foreach (var item in inspecter.Data)
                {
                    Doing = $"执行{inspecter.Site.Title} - {item.Description}检测..";

                    if (item.Result == null || item.Result.Status == EnumStatus.None)
                    {
                        var result = pluginService.GetInspectionItemResult(item);
                        item.Result = result;
                    }

                   
                    if (item.Result.Status != EnumStatus.Success)
                    {
                        this.Log().Info(
                           $" {inspecter.Title} - {item.Description} : {item.Result.Help.Join(System.Environment.NewLine)}");
                        break;
                    }
                }
            }

            watch.Stop();
            return watch.ElapsedMilliseconds;
        }
    }


    public class InspectionResultViewModel : ReactiveObject
    {
        public SiteInfo Site { get; set; }


        public string Url { get; set; }

    /// <summary>
        ///     详情
        /// </summary>
        private List<InspectionItem> _data;

        public List<InspectionItem> Data
        {
            get { return _data; }

            set { this.RaiseAndSetIfChanged(ref this._data, value); }
        }


        public string Title => $"{Site.Title}   {Url}";
    }
}