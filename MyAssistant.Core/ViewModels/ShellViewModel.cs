﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using Caliburn.Micro;
using MahApps.Metro.Controls.Dialogs;
using MyAssistant.Services;
using MyAssistant.ViewModels;
using ReactiveUI;
using Splat;

// ReSharper disable once CheckNamespace
namespace MyAssistant.ViewModels
{
    public class ShellViewModel:ReactiveObject
    {

        /// <summary>
        /// 是否是初始化
        /// </summary>
        private readonly ObservableAsPropertyHelper<bool> _isInit;

        public bool IsInit => _isInit.Value;


        /// <summary>
        /// 是否是集成
        /// </summary>
        private bool _isInspect;
        public bool IsInspect
        {
            get { return this._isInspect; }
            set { this.RaiseAndSetIfChanged(ref this._isInspect, value); }
        }


        /// <summary>
        /// 初始化视图
        /// </summary>
        public InitViewModel InitViewModel { get; private set; }

        /// <summary>
        /// 集成视图
        /// </summary>
        public InspectViewModel InspectViewModel { get; private set; }


        /// <summary>
        /// 点击初始化按钮
        /// </summary>

        public ReactiveCommand<Unit, Unit> ShowInitCommand { get; private set; }

        /// <summary>
        /// 点击集成按钮
        /// </summary>
        public ReactiveCommand<Unit, Unit> ShowInspectCommand { get; private set; }




        public ShellViewModel(
            InitViewModel initVM,
            InspectViewModel inspectVM, 
            ILibraryService service)
        {

            ShowInitCommand = ReactiveCommand.Create(() =>
            {
                IsInspect = false;
            });

            
            ShowInspectCommand = ReactiveCommand.Create(() =>
            {
                IsInspect = true;
            });


            this.WhenAnyValue(x => x.IsInspect).Select(p => !p).ToProperty(this, x => x.IsInit, out _isInit);


            InitViewModel = initVM;
            InspectViewModel = inspectVM;

            var library =  service.Get();
            IsInspect = library.InitComplete;

            InitViewModel.InitCommand.Subscribe(async finish =>
            {
                if (finish)
                {
                    IsInspect = true;

                    var l = await InspectViewModel.InspectCommond.Execute();
                    InspectViewModel.InspectCommond.Subscribe();
                }
            });
        }
    }
}
