﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MyAssistant.Core;
using MyAssistant.Management;
using MyAssistant.Model;
using MyAssistant.Services;
using MyAssistant.Utility;
using Newtonsoft.Json;
using ReactiveUI;

namespace MyAssistant.Services
{
    public class LibraryService : ILibraryService
    {

        public LibraryService()
        {
            
        }

        private static string SConfigPath => AppInfo.LibraryFilePath;

        private static readonly FileDependencyManager<Library> SCacheItem
            = new FileDependencyManager<Library>(
                files => LoadConfig(), SConfigPath);


        private static Library LoadConfig()
        {
            var vesion = typeof(LibraryService).Assembly.GetVersion().ToString();
            string text;

            if (File.Exists(SConfigPath))
            {
                text = File.ReadAllText(SConfigPath, Encoding.UTF8);
                var rtn = JsonConvert.DeserializeObject<Library>(text);
                if (rtn.Version != vesion)
                {
                    File.Delete(SConfigPath);
                }
                else
                    return rtn;
            }


            if (File.Exists(SConfigPath) == false)
            {
                var library = new Library
                {
                    Version = typeof(LibraryService).Assembly.GetVersion().ToString()
                };
                SerializeToFile(library);
            }

            text = File.ReadAllText(SConfigPath, Encoding.UTF8);
            return JsonConvert.DeserializeObject<Library>(text);
        }


        public Library Get()
        {
            return SCacheItem.Result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<SiteInfo> GetSelectedSites()
        {
            return SCacheItem.Result.Sites.FindAll(p => p.Selected);
        }

        public void Set(Library library)
        {
            SerializeToFile(library);
        }

        private static void SerializeToFile(Library library)
        {
            var json = JsonConvert.SerializeObject(library, Formatting.Indented);

            using (var targetStream = new FileStream(SConfigPath, FileMode.Create, FileAccess.Write))
            {
                using (var sw = new StreamWriter(targetStream, Encoding.UTF8, 64*1024, true))
                    sw.WriteLine(json);
            }
        }


        private void AddIISPath(EnumMode mode ,SiteInfo info)
        {
            bool display = mode == EnumMode.SignleServer;

            info.Data.Add(new SiteAttribute {Name = "站点位置", Value = "", Display = display});
        }


        private void AddLoadBalanceField(EnumMode mode, SiteInfo info)
        {
            bool display = mode == EnumMode.LoadBalance;

            info.Data.Add(new SiteAttribute {Name = "内网地址", Value = "http://", Display = display});
            info.Data.Add(new SiteAttribute {Name = "内网地址", Value = "http://", Display = display});
        }


        private void AddInfo(SiteInfo info)
        {
            info.Data.AddRange(new[]
            {
                new SiteAttribute {Name = "站点地址", Value = "http://" ,Display = true},
                new SiteAttribute {Name = "DB服务器名", Value = "" ,Display = true},
                new SiteAttribute {Name = "用户名", Value = "" , Display = true},
                new SiteAttribute {Name = "密码", Value = "" ,Display =  true},
                new SiteAttribute {Name = "数据库名", Value = "" ,Display = true}
            });
        }



        public List<SiteInfo> GetDefaultSites(EnumMode mode)
        {

            var cg = new SiteInfo
            {
                Title = "重构ERP",
                Required = true,
                Selected = true,
                Data = new ReactiveList<SiteAttribute>(),
                Type = "重构",
                BusinessClass = "NewERPInspect",
                Id = new Guid("07368264-0A71-4749-B6E5-8612A355CF4A"),
                InitRelativePath = "/Default.aspx?_mipconfig=set",
                InitInspectRelativePath = "/Default.aspx?_mipconfig=get",
                IsMain = true,
                Order = 99
            };

            AddIISPath(mode, cg);
            AddInfo(cg);
            AddLoadBalanceField(mode, cg);

             var hx = new SiteInfo
            {
                Title = "核心ERP",
                Required = true,
                Selected = true,
                Data = new ReactiveList<SiteAttribute>(),
                Type = "核心",
                BusinessClass = "OldERPInspect",
                Id = new Guid("653CAC83-E56D-4685-9513-E2DD72F372D1"),
                InitRelativePath = "/Default.aspx?_mipconfig=set",
                InitInspectRelativePath = "/Default.aspx?_mipconfig=get",
                Order = 99
            };

            AddIISPath(mode, hx);
            AddInfo(hx);
            AddLoadBalanceField(mode, hx);

            var wf = new SiteInfo
            {
                Title = "工作流站点",
                Required = true,
                Selected = true,
                Data = new ReactiveList<SiteAttribute>(),
                Type = "工作流",
                BusinessClass = "WFInspect",
                Id = new Guid("15C1EA33-63B2-4C21-BAF0-230F376C0DD7"),
                InitRelativePath = "/Default.aspx?_mipconfig=set",
                InitInspectRelativePath = "/Default.aspx?_mipconfig=wf",
                Order = 99
            };

            AddIISPath(mode, wf);
            wf.Data.Add(new SiteAttribute { Name = "站点地址", Value = "http://" ,Display = true});
            AddLoadBalanceField(mode, wf);

            var jc = new SiteInfo
            {
                Title = "集成MIP",
                Required = true,
                Selected = true,
                Data = new ReactiveList<SiteAttribute>(),
                Type = "MIP",
                BusinessClass = "MIPInspect",
                Id = new Guid("7D43E9BB-15AF-4677-876A-C403E0C10C8F"),
                InitRelativePath = "/api/Detection/SaveConfig",
                InitInspectRelativePath = "/api/Detection/GetConfig",
                Order = 1
            };

            AddIISPath(mode, jc);
            jc.Data.Add(new SiteAttribute { Name = "站点地址", Value = "http://", Display = true });

            var cz = new SiteInfo
            {
                Title = "采招系统",
                Required = false,
                Selected = true,
                Data = new ReactiveList<SiteAttribute>(),
                Type = "采招",
                BusinessClass = "CZInspect",
                Id = new Guid("6BE775E5-A151-4F06-BC3D-D0C1DCEB7F36"),
                InitRelativePath = "/Default.aspx?_mipconfig=set",
                InitInspectRelativePath = "/Default.aspx?_mipconfig=get",
                Order = 99
            };

            AddIISPath(mode, cz);
            cz.Data.Add(new SiteAttribute { Name = "站点地址", Value = "http://", Display = true });
            AddLoadBalanceField(mode, cz);

            var sites = new List<SiteInfo>
            {
                cg,
                hx,
                wf,
                jc,
                cz
            };

            return sites;
        }
    }
}