﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyAssistant.Management;
using MyAssistant.Model;
using MyAssistant.Services;
using MyAssistant.ViewModels;

namespace MyAssistant.Services
{
    public class InspectService :IInspectService
    {
        private ILibraryService _libraryService;

        public InspectService(ILibraryService libraryService)
        {
            _libraryService = libraryService;
        }

        public List<InspectionResultViewModel> GetSiteInspect(SiteInfo site)
        {
            var results = new List<InspectionResultViewModel>();

            foreach (var url in GetUrl(site, _libraryService.Get().Mode))
            {
                results.Add(new InspectionResultViewModel
                {
                    Site = site ,
                    Url = url
                });
            }

            return results;
        }


        private List<string> GetUrl(SiteInfo site, EnumMode mode)
        {
            var url = site.Data.Where(p => p.Name == "站点地址" && p.Value != "" && p.Value != "http://").Select(p => p.Value).ToList();

            List<string> balanceUrl = null;

            if (mode == EnumMode.LoadBalance)
            {
                balanceUrl =
                    site.Data.Where(p => p.Name == "内网地址" && p.Value != "http://" && p.Value != "")
                        .Select(p => p.Value).ToList();

                balanceUrl.AddRange(url);
            }

            return balanceUrl?.Count > 0 ? balanceUrl.Distinct().ToList() : url;
        }
    }
}
