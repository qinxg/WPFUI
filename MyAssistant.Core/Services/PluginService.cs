﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using MyAssistant.Management;
using MyAssistant.Model;
using MyAssistant.Plugin;

namespace MyAssistant.Services
{
    
    /// <summary>
    /// 插件服务
    /// </summary>
    public class PluginService : IPluginService
    {
        private PluginBase Instance { get; set; }

        public PluginService(PluginBase instance)
        {
            Instance = instance;
        }


        /// <summary>
        /// 设置Url
        /// </summary>
        /// <param name="url"></param>
        /// <param name="title"></param>
        /// <param name="library"></param>
        public void SetType(string url,string title,Library library)
        {
            Instance.Context = new PluginContext
            {
                Title = title,
                Url = url,
                Library = library
            };
        }

        public string GetTitle()
        {
            return Instance.Context.Title;
        }

        /// <summary>
        /// 获取站点检查项
        /// </summary>
        /// <returns></returns>
        public List<InspectionItem> LoadSiteInspectionItem()
        {
            return Instance.GetInspectionItems();
        }

        /// <summary>
        /// 初始化
        /// </summary>
        /// <returns></returns>
        public Task<HttpStatus> Init()
        {
            return Task.Run(() => Instance.Init());
        }

        /// <summary>
        /// 检查配置是否正确
        /// </summary>
        /// <returns></returns>
        public Task<bool> CheckConfig()
        {
            return Task.Run(() => Instance.CheckConfig());
        }

        public void BeforeInspect()
        {
            Instance.BeforeInspect();
        }

        /// <summary>
        /// 获取检查项的结果
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public InspectionResult GetInspectionItemResult(InspectionItem item)
        {
            var methodInfo =
                Instance
                    .GetType().GetMethods().Select(p => new
                    {
                        Attr = p.GetCustomAttribute<InspectPluginMethodAttribute>(),
                        Method = p
                    }).FirstOrDefault(p => p.Attr != null && p.Attr.Id == item.Id);

            InspectionResult result;

            if (methodInfo == null)
                result = Instance.GetDynamicInspectionResult(item.Id);
            else
            {
                var rtn = methodInfo.Method.Invoke(Instance, null);
                result = rtn as InspectionResult;
            }
            return result;
        }
    }
}