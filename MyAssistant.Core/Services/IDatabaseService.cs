﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyAssistant.Management;
using MyAssistant.Model;

namespace MyAssistant.Services
{
    public interface IDatabaseService
    {
        List<string> GetAllDatabase(DatabaseInfo info);

        bool ExecCommand(Library library);
    }
}
