﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyAssistant.Management;
using MyAssistant.Model;

namespace MyAssistant.Services
{
    public interface IPluginService
    {
        /// <summary>
        /// 获取检测项
        /// </summary>
        /// <returns></returns>
        List<InspectionItem> LoadSiteInspectionItem();

        /// <summary>
        /// 初始化
        /// </summary>
        /// <returns></returns>
        Task<HttpStatus> Init();

        /// <summary>
        /// 获取检测项的结果
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        InspectionResult GetInspectionItemResult(InspectionItem item);


        void SetType(string url, string title, Library library);


        string GetTitle();


        Task<bool> CheckConfig();
        void BeforeInspect();
    }
}
