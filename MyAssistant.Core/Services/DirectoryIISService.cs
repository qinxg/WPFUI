﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using MyAssistant.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Win32;
using MyAssistant.Utility.IIS;
using MyAssistant.Utility;

namespace MyAssistant.Services
{
    /// <summary>
    /// IIS 相关操作Service服务
    /// </summary>
    public class DirectoryIISService : IDirectoryIISService
    {
        /// <summary>
        /// 所有站点基本信息
        /// </summary>
        /// <returns></returns>
        public List<IISSite> GetALLSites()
        {
            return IISHelper.GetAllSites();
        }

        /// <summary>
        /// 初始化虚拟目录
        /// </summary>
        /// <param name="mainSite">主站点名称</param>
        /// <param name="currentSite">当前站点名称</param>
        public void InitVirtualDirectory(string mainSite, string currentSite)
        {
            var mainSiteInfo =
                IISHelper.GetAllVirtualDirectoryByName(mainSite)
                    .SingleOrDefault(item => item.VirtualDirectoryName == "");
            var currentSiteInfo =
                IISHelper.GetAllVirtualDirectoryByName(currentSite)
                    .SingleOrDefault(item => item.VirtualDirectoryName == "");

            IISVirtualDirectory vir = new IISVirtualDirectory();
            vir.SiteID = currentSiteInfo.SiteID;
            vir.VirtualDirectoryName = "upfiles";
            vir.VirtualDirectoryPath = string.Concat(mainSiteInfo.VirtualDirectoryPath, "\\UpFiles");

            IISHelper.InitVirtualDirectory(vir);
        }


        public  bool ExistVirualPath(string siteName,string vdirName)
        {
            var site = IISHelper.GetAllVirtualDirectoryByName(siteName);
            var vDir =
                site.FirstOrDefault(
                    item =>
                        String.Compare(item.VirtualDirectoryName, vdirName,
                            StringComparison.InvariantCultureIgnoreCase) == 0);

            if (vDir != null)
                return true;
            return false;
        }


        /// <summary>
        /// 检查虚拟目录是否存在 &路径是否正确
        /// </summary>
        /// <param name="mainSite">主站点名称</param>
        /// <param name="currentSite">当前站点名称</param>
        /// <returns></returns>
        public bool IsExistsVirtualDirectory(string mainSite, string currentSite)
        {
            string virtualName = "upfiles";
            //判断ERP站点upfiles 与工作流站点虚拟目录地址是否一致
            var erpSite =
                IISHelper.GetAllVirtualDirectoryByName(mainSite)
                    .FirstOrDefault(item => item.VirtualDirectoryName == "");

            var currentSiteInfo =
                IISHelper.GetAllVirtualDirectoryByName(currentSite)
                    .FirstOrDefault(item =>
                        string.Compare(item.VirtualDirectoryName, virtualName,
                            StringComparison.CurrentCultureIgnoreCase) == 0);

            if (erpSite == null || currentSiteInfo == null)
                return false;


            //判断当前站点是否映射虚拟目录upfiles
            bool IsExists = IISHelper.IsExistsVirtualDirectory(currentSiteInfo.SiteID, virtualName);
            if (!IsExists)
            {
                return false;
            }

            erpSite.VirtualDirectoryPath = string.Concat(erpSite.VirtualDirectoryPath, "\\UpFiles");

            if (string.Compare(erpSite.VirtualDirectoryPath, currentSiteInfo.VirtualDirectoryPath,
                    StringComparison.CurrentCultureIgnoreCase) != 0)
            {
                return false;
            }
            return true;
        }

        public ERPInfo GetErpInfo(string siteName,bool iscg)
        {
            var site = IISHelper.GetAllSites().FirstOrDefault(p => p.SiteName == siteName);
            var info = new ERPInfo
            {
                ERPSite = "http://"
            };
            if (site == null)
                return info;

            var port = site.SitePorts.Count > 0 ? site.SitePorts[0] : "1";
            info.ERPSite = $"http://{IPAddressHelper.GetLocalMachineIP()}:{port}";

            var lincensePath = site.SitePath + "\\bin\\License.xml";

            if (!File.Exists(lincensePath))
                return info;

            XmlDocument xml = new XmlDocument();
            xml.Load(lincensePath);
            if (xml.DocumentElement == null)
                return info;

            var dname = xml.DocumentElement.Name;
            string xpath = "/product/dbconns/dbconn[@appname='Default']";
            //判断ERP 251以下License版本
            xpath = (dname == "product" ? xpath : "/license/data" + xpath);
            var selectSingleNode = xml.SelectSingleNode(xpath);
            if (selectSingleNode?.Attributes == null)
                return info;

            var regname = selectSingleNode.Attributes["regname"].Value;

            if (iscg)
                SetNewDBInfo(info, regname);
            else
                SetDBInfo(info, regname);


            return info;
        }


        private void SetDBInfo(ERPInfo erpConfig, string regname)
        {
            string regPath = "software\\mysoft\\" + regname;
            RegistryKey key = Registry.LocalMachine.OpenSubKey(regPath, true);
            if (key != null)
            {
                erpConfig.DBAddress = key.GetValue("ServerName").ToString();
                erpConfig.DBName = key.GetValue("DBName").ToString();
                erpConfig.DBUser = key.GetValue("UserName").ToString();
                erpConfig.DBPwd = DBConnectCheck.DeCode(key.GetValue("SaPassword").ToString());
            }
        }


        private void SetNewDBInfo(ERPInfo erpConfig, string regname)
        {
            var settings = GetSettings(regname);
            if (settings != null && settings.Count != 0)
            {
                erpConfig.DBAddress = settings.GetValue("Server");
                erpConfig.DBName = settings.GetValue("Database").ToString();
                erpConfig.DBUser = settings.GetValue("UserId").ToString();
                erpConfig.DBPwd = DBConnectCheck.DeCode(settings.GetValue("Password").ToString());
            }
        }


        private Dictionary<string, string> GetSettings(string regname,string connectionName = "masterDb")
        {
            // 为了简化注册表的读取方法，这里返回某个路径下的所有键值
            Dictionary<string, string> dict = new Dictionary<string, string>(8, StringComparer.OrdinalIgnoreCase);


            // 计算注册表路径
            string regPath = @"SOFTWARE\mysoft\erp-m6\"     // 新ERP的注册表根路径
                            + regname                        // lincense 中的连接名称
                            + (string.IsNullOrEmpty(connectionName)
                                    ? string.Empty          // 根节点保存数据库类型信息
                                    : "\\" + connectionName);   // masterDb/queryDb 具体的连接参数

            //如果是64位系统，不管当前进程是32位还是64位，只使用64位注册表地址。
            if (Environment.Is64BitOperatingSystem)
            {
                using (RegistryKey localMachine64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                {
                    using (RegistryKey key = localMachine64.OpenSubKey(regPath, false))
                    {
                        FillRegistryValues(key, dict);
                    }
                }
            }
            else
            {
                using (RegistryKey key = Registry.LocalMachine.OpenSubKey(regPath, false))
                {
                    FillRegistryValues(key, dict);
                }
            }

            return dict;
        }

        private void FillRegistryValues(RegistryKey key, Dictionary<string, string> dict)
        {
            if (key != null)
            {
                string[] names = key.GetValueNames();
                foreach (string name in names)
                {
                    string value = key.GetValue(name, string.Empty).ToString();
                    dict[name] = value;
                }
            }
        }

    }

    /// <summary>
    /// DirectoryIISService单元测试
    /// </summary>
    [TestClass]
    public class TestDirectoryIISService
    {
        DirectoryIISService dis = new DirectoryIISService();

        /// <summary>
        /// TestGetALLSites
        /// </summary>
        [TestMethod]
        public void TestGetALLSites()
        {
            List<IISSite> sites = new List<IISSite>();
            //测试IIS6
            //IISHelper.InitializeVersion(IISProviderType.IIS6);
            //sites= IISHelper.GetALLSites();

            //测试IIS7
            //IISHelper.InitializeVersion(IISProviderType.IIS7);
            sites = IISHelper.GetAllSites();
            Assert.IsFalse(sites.Count() <= 0, "错误");
        }

        /// <summary>
        /// 根据站点添加虚拟目录
        /// </summary>
        [TestMethod]
        public void InitVirtualDirectory()
        {
            string siteId = "27";
            string virtualDirectoryName = "upfiles";
            //erp路径 E:\产品发布更新包1111\ERP308\ERP\UpFiles
            //D:\TFS\住宅ERP\源代码\Trunk\ERP\明源整体解决方案\Map\UpFiles
            string virtualDirectoryPath = @"E:\产品发布更新包1111\ERP308\ERP\UpFiles"; 
            
            IISVirtualDirectory iisVir = new IISVirtualDirectory()
            {
                SiteID = siteId,
                VirtualDirectoryName = virtualDirectoryName,
                VirtualDirectoryPath = virtualDirectoryPath
            };

            //测试IIS6
            //IISHelper.InitializeVersion(IISProviderType.IIS6);

            //测试IIS7
            //IISHelper.InitializeVersion(IISProviderType.IIS7);

            IISHelper.InitVirtualDirectory(iisVir);
            Assert.IsTrue(true);
        }

        /// <summary>
        /// 根据站点添加虚拟目录
        /// </summary>
        [TestMethod]
        public void Test_InitVirtualDirectory()
        {
            string mainSite = "ERP308";
            string currentSite = "ERP351";

            //测试IIS6
            //IISHelper.InitializeVersion(IISProviderType.IIS6);

            //测试IIS7
            //IISHelper.InitializeVersion(IISProviderType.IIS7);

            dis.InitVirtualDirectory(mainSite, currentSite);
            Assert.IsTrue(true);
        }

        /// <summary>
        /// 检查虚拟目录是存在
        /// </summary>
        [TestMethod]
        public void IsExistsVirtualDirectory()
        {
            string erpSiteID = "27";
            string virtualDirectoryName = "upfiles";

            //IISHelper.InitializeVersion(IISProviderType.IIS6);
            //IISHelper.InitializeVersion(IISProviderType.IIS7);
            bool flag = IISHelper.IsExistsVirtualDirectory(erpSiteID, virtualDirectoryName);

            Assert.IsTrue(flag, string.Format("erpSiteID:{0}检查虚拟目录不存在",erpSiteID));
        }

        /// <summary>
        ///  检查虚拟目录是否存在 &路径是否正确
        /// </summary>
        [TestMethod]
        public void TestService_IsExistsVirtualDirectory()
        {
            string erpSiteID = "6";
            string workFlowSiteID = "27";

            //IISHelper.InitializeVersion(IISProviderType.IIS6);
            //IISHelper.InitializeVersion(IISProviderType.IIS7);
            bool flag=dis.IsExistsVirtualDirectory(erpSiteID, workFlowSiteID);

            Assert.IsTrue(flag, "虚拟目录不存在或映射upfiles路径不正确");
        }

        /// <summary>
        /// 根据SiteID查询所有虚拟目录
        /// </summary>
        [TestMethod]
        public void GetAllVirtualDirectory()
        {
            string siteID = "27";
            var virs = IISHelper.GetAllVirtualDirectoryById(siteID);
            //Console.WriteLine(virs.Count());
            Assert.IsTrue(true);
        }
    }
}
