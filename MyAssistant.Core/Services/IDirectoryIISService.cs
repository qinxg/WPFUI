﻿using MyAssistant.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAssistant.Services
{
    public interface IDirectoryIISService
    {
        /// <summary>
        /// 所有站点基本信息
        /// </summary>
        /// <returns></returns>
        List<IISSite> GetALLSites();


        /// <summary>
        /// 初始化虚拟目录
        /// </summary>
        /// <param name="mainSite">主站点名称</param>
        /// <param name="currentSite">当前站点名称</param>
        void InitVirtualDirectory(string mainSite , string currentSite);

        /// <summary>
        /// 检查虚拟目录是否存在 &路径是否正确
        /// </summary>
        /// <param name="mainSite">主站点名称</param>
        /// <param name="currentSite">当前站点名称</param>
        /// <returns></returns>
        bool IsExistsVirtualDirectory(string mainSite, string currentSite);


        bool ExistVirualPath(string siteName, string vdirName);

        ERPInfo GetErpInfo(string siteName, bool iscg);

    }
}
