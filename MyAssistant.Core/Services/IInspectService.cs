﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyAssistant.Management;
using MyAssistant.Model;
using MyAssistant.ViewModels;

namespace MyAssistant.Services
{
    public interface IInspectService
    {
        List<InspectionResultViewModel> GetSiteInspect(SiteInfo site);
    }

}