﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyAssistant.Management;
using MyAssistant.Model;

namespace MyAssistant.Services
{
    /// <summary>
    /// 
    /// </summary>
    public interface  ILibraryService
    {

        List<SiteInfo> GetSelectedSites();
        Library Get();
        void Set(Library library);
        List<SiteInfo> GetDefaultSites(EnumMode mode);
    }
}
