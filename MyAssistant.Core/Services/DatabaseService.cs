﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MyAssistant.Core;
using MyAssistant.Management;
using MyAssistant.Model;
using MyAssistant.Tokens;
using Splat;

namespace MyAssistant.Services
{
    public class DatabaseService : IDatabaseService ,IEnableLogger
    {
        private const string Query =
            "SELECT name FROM sys.databases WHERE name NOT IN (\'master\', \'tempdb\', \'model\', \'msdb\') and source_database_id IS NULL ;";

        public List<string> GetAllDatabase(DatabaseInfo info)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(GetConnectionString(info)))
                {
                    conn.Open();
                    return conn.Query<string>(Query).ToList();
                }
            }
            catch (Exception ex)
            {
                this.Log().FatalException(ex.Message, ex);
            }
            return null;
        }

        public bool ExecCommand(Library library)
        {
            var token = new Tokenizer(new TokenManager());
            
            var files = Directory.GetFiles(Path.Combine(Path.GetDirectoryName(AppInfo.RunPath), "Data", "SQL"));

            foreach (var fileName in files)
            {
                var text = File.ReadAllText(fileName);
                var exec = token.Replace(text, library);

                var type = Path.GetFileNameWithoutExtension(fileName);
                var dbinfo = GetDatabaseInfo(library.Sites.FirstOrDefault(p => p.Type == type));


                using (SqlConnection conn = new SqlConnection(GetConnectionString(dbinfo)))
                {
                    conn.Open();
                    this.Log().Info(exec);
                    conn.Execute(exec);
                    conn.Close();
                }
            }

            return true;
        }



        private DatabaseInfo GetDatabaseInfo(SiteInfo site)
        {
            var data = site.Data;

            return new DatabaseInfo
            {
                Server = data.FirstOrDefault(q => q.Name == "DB服务器名")?.Value,
                Password = data.FirstOrDefault(q => q.Name == "密码")?.Value,
                User = data.FirstOrDefault(q => q.Name == "用户名")?.Value,
                InitialCatalog = data.FirstOrDefault(q => q.Name == "数据库名")?.Value
            };
        }

        private string GetConnectionString(DatabaseInfo info)
        {
            var builder = new SqlConnectionStringBuilder
            {
                DataSource = info.Server,
                UserID = info.User,
                Password = info.Password,
                ConnectTimeout = 1
            };

            if (!string.IsNullOrEmpty(info.InitialCatalog))
                builder.InitialCatalog = info.InitialCatalog;
            
            return builder.ConnectionString;
        }
    }
}
