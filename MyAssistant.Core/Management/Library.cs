﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyAssistant.Model;
using MyAssistant.Utility;
using Newtonsoft.Json;
using ReactiveUI;
using Splat;

namespace MyAssistant.Management
{
    [Serializable]
    public class Library : ReactiveObject
    {
        [JsonProperty("sites")]
        //站点信息
        public List<SiteInfo> Sites { get; set; }

        [JsonProperty("initComplete")]
        public bool InitComplete { get; set; } = false;

        /// <summary>
        /// 最后初始化时间
        /// </summary>
        [JsonProperty("lastInitTime")]
        public  DateTime? LastInitTime { get; set; }

        /// <summary>
        /// 最后检查时间
        /// </summary>
        [JsonProperty("lastIspectTime")]
        public DateTime? LastIspectTime { get; set; }

        /// <summary>
        /// 版本
        /// </summary>
        [JsonProperty("version")]
        public  string Version { get; set; }


        [JsonProperty("model")]
        public EnumMode Mode { get; set; }
    }

    public enum EnumMode
    {
        SignleServer = 0,
        MultiServer = 1,
        LoadBalance = 2
    }
}