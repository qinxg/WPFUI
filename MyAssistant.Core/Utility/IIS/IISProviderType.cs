﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAssistant.Utility.IIS
{
    /// <summary>
    /// IIS操作类型
    /// </summary>
    /// <remarks>
    /// IIS操作类型，会根据不同的服务提供者类型
    /// </remarks>
    internal enum IISProviderType
    {
        /// <summary>
        /// IIS6
        /// </summary>
        IIS6 = 0,

        /// <summary>
        /// IIS7
        /// </summary>
        IIS7 = 1,


        /// <summary>
        /// 未知类型
        /// </summary>
        None = 2
    }
}
