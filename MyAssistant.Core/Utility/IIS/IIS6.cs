﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyAssistant.Model;
using System.DirectoryServices;

namespace MyAssistant.Utility.IIS
{
    internal class IIS6 : IDirectoryIIS
    {
        //缓存所有站点
        static List<IISSite> Sites = new List<IISSite>();
        private DirectoryEntry siteEntry;
        public IIS6()
        {
            Sites = GetAllSites();
        }


        public List<IISSite> GetAllSites()
        {
            List<IISSite> sites = new List<IISSite>();

            siteEntry = new DirectoryEntry("IIS://localhost/w3svc");

            siteEntry.Children.Cast<DirectoryEntry>().ForEach(childEntry =>
            {
                if (childEntry.SchemaClassName == "IIsWebServer")
                {
                    IISSite iisSite = new IISSite();
                    if (childEntry.Properties["ServerComment"].Value != null)
                    {
                        string siteName = childEntry.Properties["ServerComment"].Value.ToString();
                        var propertyCollection = childEntry.Properties["ServerBindings"];
                        if (propertyCollection.Value.GetType() == typeof(string))
                        {
                            iisSite.SitePorts = new List<string>() { propertyCollection.Value.ToString().Trim(':') };
                        }
                        else if (propertyCollection.Value.GetType() == typeof(object[]))
                        {
                            object[] prots = (object[])propertyCollection.Value;
                            List<string> lst = new List<string>();
                            foreach (var o in prots)
                            {
                                lst.Add(o.ToString().Trim(':'));
                            }
                            iisSite.SitePorts = lst;
                        }
                        iisSite.SiteID = childEntry.Name;
                        iisSite.SiteName = siteName;
                        DirectoryEntry path = new DirectoryEntry(string.Format("IIS://LOCALHOST/W3SVC/{0}/root", childEntry.Name));
                        iisSite.SitePath = path.Properties["Path"].Value.ToString();
                        sites.Add(iisSite);
                    }
                }
            });
            return sites.OrderBy(item=>item.SiteName).ToList();
        }

        public List<IISVirtualDirectory> GetAllVirtualDirectoryById(string siteID)
        {
            List<IISVirtualDirectory> virs = new List<IISVirtualDirectory>();

            string metabasePath = string.Format("IIS://localhost/w3svc/{0}/root", siteID);
            siteEntry = new DirectoryEntry(metabasePath);

            siteEntry.Children.Cast<DirectoryEntry>().ForEach(childEntry =>
            {
                if (childEntry.SchemaClassName == "IIsWebVirtualDir" )
                {
                    virs.Add(new IISVirtualDirectory()
                    {
                        SiteID = siteID,
                        VirtualDirectoryName = childEntry.Name,
                        VirtualDirectoryPath = childEntry.Properties["Path"].Value.ToString(),
                    });
                }
            });

            //添加本身站点
            virs.Add(new IISVirtualDirectory() {
                SiteID=siteID,
                VirtualDirectoryName="",
                VirtualDirectoryPath= siteEntry.Properties["Path"].Value.ToString()
            });

            return virs;
         }


        /// <summary>
        /// 通过siteName获取站点标识
        /// </summary>
        /// <param name="siteName"></param>
        /// <returns></returns>
        string GetSiteIDByName(string siteName)
        {
            IISSite site = Sites.SingleOrDefault(item => item.SiteName.IsIgnoreCaseEqualTo(siteName));
            if (site == null) {
                throw new Exception("没有找到此站点名称!"+ siteName);
            }
            return site.SiteID;
        }

        public List<IISVirtualDirectory> GetAllVirtualDirectoryByName(string siteName)
        {
            string siteID = GetSiteIDByName(siteName);
            return GetAllVirtualDirectoryById(siteID);
        }

        public void InitVirtualDirectory(IISVirtualDirectory virtualDirectory)
        {
            string metabasePath = string.Format("IIS://localhost/w3svc/{0}/root", virtualDirectory.SiteID);

            using (siteEntry = new DirectoryEntry(metabasePath))
            {
                DirectoryEntry newVDir = null;
                try
                {
                    //find 为空 抛出异常 忽略
                    newVDir = siteEntry.Children.Find(virtualDirectory.VirtualDirectoryName, "IIsWebVirtualDir");
                }
                catch (Exception)
                {
                    //throw new Exception("找不到指定路径");
                }
                if (newVDir == null)
                {
                    //异常 添加虚拟目录 
                    newVDir = siteEntry.Children.Add(virtualDirectory.VirtualDirectoryName, "IIsWebVirtualDir");
                }
                newVDir.Properties["Path"][0] = virtualDirectory.VirtualDirectoryPath;
                newVDir.CommitChanges();
            }
        }

        public bool IsExistsVirtualDirectory(string siteID, string virtualDirectoryName)
        {
            bool flag = true;
            string metabasePath = string.Format("IIS://localhost/w3svc/{0}/root", siteID);
            siteEntry = new DirectoryEntry(metabasePath);
            try
            {
                //find 为空 抛出异常 忽略
                siteEntry.Children.Find(virtualDirectoryName, "IIsWebVirtualDir");
            }
            catch (Exception)
            {
                flag = false;
            }
            return flag;
        }
    }
}
