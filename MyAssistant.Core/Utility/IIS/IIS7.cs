﻿using System;
using System.Collections.Generic;
using System.Linq;
using MyAssistant.Model;
using Microsoft.Web.Administration;

namespace MyAssistant.Utility.IIS
{
    internal class IIS7 : IDirectoryIIS
    {
        private ServerManager server;
        public IIS7()
        {
            server = new ServerManager();
        }

        public List<IISSite> GetAllSites()
        {
            List<IISSite> sites = new List<IISSite>();
            //var site1 = server.Sites.SingleOrDefault(item => item.Id.ToString() == "27");
            server.Sites.ForEach(site =>
            {
                IISSite iisSite = new IISSite();
                string siteName = site.Name;
                string sitePath = site.Applications["/"].VirtualDirectories["/"].PhysicalPath;
                List<string> ports = new List<string>();
                if (site.Bindings != null)
                {
                    site.Bindings.ForEach(netBind =>
                    {
                        if (netBind != null && netBind.EndPoint != null
                           && !ports.Contains(netBind.EndPoint.Port.ToString()))
                        {
                            ports.Add(netBind.EndPoint.Port.ToString());
                        }
                    });
                }
                iisSite.SiteID = site.Id.ToString();
                iisSite.SiteName = siteName;
                iisSite.SitePath = sitePath;
                iisSite.SitePorts = ports;
                sites.Add(iisSite);
            });
            return sites.OrderBy(item => item.SiteName).ToList();
        }

        public List<IISVirtualDirectory> GetAllVirtualDirectoryById(string siteID)
        {
            var site = server.Sites.SingleOrDefault(item => item.Id.ToString() == siteID);
            return GetAllVirtualDirectory(site);
        }

        public List<IISVirtualDirectory> GetAllVirtualDirectoryByName(string siteName)
        {
            var site = server.Sites.SingleOrDefault(item => item.Name.IsIgnoreCaseEqualTo(siteName));
            if (site == null) {
                throw new Exception("没有找到此站点名称!" + siteName);
            }
            return GetAllVirtualDirectory(site);
        }

        List<IISVirtualDirectory> GetAllVirtualDirectory(Site site)
        {
            List<IISVirtualDirectory> virs = new List<IISVirtualDirectory>();
            var virtualDirectories = site.Applications["/"].VirtualDirectories;

            virtualDirectories.ForEach(vir => {
                virs.Add(new IISVirtualDirectory()
                {
                    SiteID = site.Id.ToString(),
                    VirtualDirectoryName = vir.Path.Trim('/'),
                    VirtualDirectoryPath = vir.PhysicalPath
                });
            });
            return virs;
         }


        public void InitVirtualDirectory(IISVirtualDirectory virtualDirectory)
        {
            virtualDirectory.VirtualDirectoryName = string.Concat("/", virtualDirectory.VirtualDirectoryName);
            var site = server.Sites.SingleOrDefault(item => item.Id.ToString() == virtualDirectory.SiteID);
            var virtualDirectories = site.Applications["/"].VirtualDirectories;
            var vir = virtualDirectories.SingleOrDefault(item => item.Path.IsIgnoreCaseEqualTo(virtualDirectory.VirtualDirectoryName));
            if (vir == null)
            {
                site.Applications["/"].VirtualDirectories.Add(virtualDirectory.VirtualDirectoryName, virtualDirectory.VirtualDirectoryPath);
            }
            else {
                vir.PhysicalPath = virtualDirectory.VirtualDirectoryPath;
            }
            server.CommitChanges();
        }

        public bool IsExistsVirtualDirectory(string siteID, string virtualDirectoryName)
        {
            bool flag = true;
            virtualDirectoryName = string.Concat("/", virtualDirectoryName);
            var site = server.Sites.SingleOrDefault(item => item.Id.ToString() == siteID);
            var virtualDirectories = site.Applications["/"].VirtualDirectories;
            if (virtualDirectories.SingleOrDefault(item => item.Path.IsIgnoreCaseEqualTo(virtualDirectoryName)) == null)
            {
                flag = false;
            }
            return flag;
        }
    }
}
