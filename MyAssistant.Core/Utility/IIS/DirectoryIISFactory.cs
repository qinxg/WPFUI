﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAssistant.Utility.IIS
{
    internal class DirectoryIISFactory : IDirectoryIISFactory
    {
        public IDirectoryIIS Create(IISProviderType providerType)
        {
            switch (providerType)
            {
                case IISProviderType.IIS6:
                    return new IIS6();

                case IISProviderType.IIS7:
                    return new IIS7();
                default:
                    throw new Exception("提供者类型:不存在ProviderType");
            }
        }
    }


    internal interface IDirectoryIISFactory {
        /// <summary>
        /// 创建
        /// </summary>
        /// <param name="providerType">服务提供者类型</param>
        /// <returns>IIS转换器接口实例</returns>
        IDirectoryIIS Create(IISProviderType providerType);
    }
}
