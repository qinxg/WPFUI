﻿using MyAssistant.Model;
using System;
using System.Collections.Generic;

namespace MyAssistant.Utility.IIS
{
    public class IISHelper
    {
        /// <summary>
        /// 当前工厂实例
        /// </summary>
        static IDirectoryIISFactory directoryIisFactory = new DirectoryIISFactory();

        /// <summary>
        /// IIS接口实例
        /// </summary>
        static IDirectoryIIS directoryIis;

        static IISHelper() {
            int version = GetIISVersion();
            directoryIis = directoryIisFactory.Create(ConvertType(version));
        }

        /// <summary>
        /// 初始化 应于测试
        /// </summary>
        /// <param name="type">枚举类型</param>
        internal static void InitializeVersion(IISProviderType type)
        {
            directoryIis = directoryIisFactory.Create(type);
        }

        /// <summary>
        /// 检查IIS版本,注册表方式
        /// </summary>
        /// <returns></returns>
        static int GetIISVersion()
        {
            string mjValue = RegistryHelper.GetLocalMachineKeyValue("SOFTWARE\\Microsoft\\InetStp", "MajorVersion");
            if (string.IsNullOrEmpty(mjValue))
            {
                throw new Exception("本地服务器未安装IIS。");
            }
            return int.Parse(mjValue);
        }

        /// <summary>
        /// 判断转换
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        static IISProviderType ConvertType(int version) {
            IISProviderType type = IISProviderType.None;
            switch (version)
            {
                case 6:
                    type = IISProviderType.IIS6;
                    break;
                case 7:
                    type = IISProviderType.IIS7;
                    break;
                default:
                    type = IISProviderType.IIS7;
                    break;
            }
            return type;
        }

        /// <summary>
        /// 所有站点信息集合
        /// </summary>
        /// <returns></returns>
        public static List<IISSite> GetAllSites()
        {
            return directoryIis.GetAllSites();
        }


        /// <summary>
        /// 初始化虚拟目录
        /// </summary>
        /// <param name="siteName"></param>
        /// <param name="virtualDirectoryName"></param>
        /// <returns></returns>
        public static void InitVirtualDirectory(IISVirtualDirectory virtualDirectory)
        {
           directoryIis.InitVirtualDirectory(virtualDirectory);
        }

        /// <summary>
        /// 是否存在虚拟目录
        /// </summary>
        /// <param name="siteID">当前站点标识</param>
        /// <param name="virtualDirectoryName">虚拟目录名称</param>
        /// <returns></returns>
        public static bool IsExistsVirtualDirectory(string siteID,string virtualDirectoryName)
        {
            return directoryIis.IsExistsVirtualDirectory(siteID, virtualDirectoryName);
        }

        /// <summary>
        /// 根据站点标识查询所有虚拟目录集合
        /// </summary>
        /// <param name="siteID"></param>
        /// <returns></returns>
        public static List<IISVirtualDirectory> GetAllVirtualDirectoryById(string siteID)
        {
            return directoryIis.GetAllVirtualDirectoryById(siteID);
        }

        /// <summary>
        /// 根据站点标识查询所有虚拟目录 和当前站点集合 
        /// </summary>
        /// <param name="siteID"></param>
        /// <returns></returns>
        public static List<IISVirtualDirectory> GetAllVirtualDirectoryByName(string siteName)
        {
            return directoryIis.GetAllVirtualDirectoryByName(siteName);
        }
    }
}
