﻿using MyAssistant.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAssistant.Utility.IIS
{
    internal interface IDirectoryIIS
    {
        /// <summary>
        /// 所有站点信息集合
        /// </summary>
        /// <returns></returns>
        List<IISSite> GetAllSites();

        /// <summary>
        /// 初始化虚拟目录
        /// </summary>
        /// <param name="virtualDirectory"></param>
        void InitVirtualDirectory(IISVirtualDirectory virtualDirectory);

        /// <summary>
        /// 通过判断虚拟目录是否存在
        /// </summary>
        /// <param name="siteID"></param>
        /// <param name="virtualDirectoryName"></param>
        /// <returns></returns>
        bool IsExistsVirtualDirectory(string siteID, string virtualDirectoryName);

        /// <summary>
        /// 通过站点标识找到所有虚拟目录
        /// </summary>
        /// <param name="siteID"></param>
        /// <returns></returns>
        List<IISVirtualDirectory> GetAllVirtualDirectoryById(string siteID);

        /// <summary>
        /// 通过站点名称找到所有虚拟目录 和当前站点集合  
        /// </summary>
        /// <param name="siteName"></param>
        /// <returns></returns>
        List<IISVirtualDirectory> GetAllVirtualDirectoryByName(string siteName);
    }
}
