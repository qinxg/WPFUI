﻿using System;
using System.IO;
using System.Text;

namespace MyAssistant.Utility
{
    
    public class DBConnectCheck
    {
        //功能：字符串解密
        //参数：待反加密串
        //返回：反加密变换后的结果
        public static string DeCode(string inStr)
        {
            string StrBuff = null;
            int IntLen = 0;
            int IntCode = 0;
            int IntCode1 = 0;
            int IntCode2 = 0;
            int IntCode3 = 0;

            StrBuff = "";

            IntLen = inStr.Trim().Length;

            IntCode1 = IntLen % 3;
            IntCode2 = IntLen % 9;
            IntCode3 = IntLen % 5;

            if (IntLen / 2.0 == IntLen / 2)
            {
                IntCode = IntCode2 + IntCode3;
            }
            else
            {
                IntCode = IntCode1 + IntCode3;
            }


            for (int i = 1; i <= IntLen; i++)
            {
                StrBuff = StrBuff + Convert.ToChar(Asc(Convert.ToChar(inStr.Substring(IntLen - i, 1))) + IntCode);

                if (IntCode == IntCode1 + IntCode3)
                {
                    IntCode = IntCode2 + IntCode3;
                }
                else
                {
                    IntCode = IntCode1 + IntCode3;
                }
            }
            for (int i = 0; i < inStr.Length - IntLen; ++i)
                StrBuff += " ";

            return StrBuff;
        }
        public static int Asc(char c)
        {
            int converted = c;
            if (converted >= 0x80)
            {
                byte[] buffer = new byte[2];
                // if the resulting conversion is 1 byte in length, just use the value
                if (System.Text.Encoding.Default.GetBytes(new char[] { c }, 0, 1, buffer, 0) == 1)
                {
                    converted = buffer[0];
                }
                else
                {
                    // byte swap bytes 1 and 2;
                    converted = buffer[0] << 16 | buffer[1];
                }
            }
            return converted;
        }
    }
}
