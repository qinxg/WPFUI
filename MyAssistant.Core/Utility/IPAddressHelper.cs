﻿using System.Diagnostics;
using System.Net;
using System.Text.RegularExpressions;

namespace MyAssistant.Utility
{
    public class IPAddressHelper
    {
        public static string GetLocalMachineIP()
        {
            string ip = string.Empty;

            IPAddress[] adds = Dns.GetHostAddresses(Dns.GetHostName());
            string localIP = GetLocalIP();

            foreach (IPAddress ips in adds)
            {
                string ipstr = ips.ToString();
                if (ipstr.Split('.').Length == 4 && localIP == ipstr)
                {
                    ip = ipstr;
                }
            }

            return ip;
        }


        private static string GetLocalIP()
        {
            string result = RunApp("route", "print");
            Match m = Regex.Match(result, "0.0.0.0\\s+0.0.0.0\\s+(\\d+.\\d+.\\d+.\\d+)\\s+(\\d+.\\d+.\\d+.\\d+)");
            if (m.Success)
            {
                return m.Groups[2].Value;
            }
            return string.Empty;
        }

        public static string RunApp(string filename, string arguments)
        {
            Process process = new Process();
            process.StartInfo.FileName = filename;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.Arguments = arguments;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.UseShellExecute = false;
            process.Start();
            using (System.IO.StreamReader sr = new System.IO.StreamReader(process.StandardOutput.BaseStream, System.Text.Encoding.Default))
            {
                string strTxt = sr.ReadToEnd();
                sr.Close();
                if (!process.HasExited)
                {
                    process.Kill();
                }
                return strTxt;
            }
        }

    }
}
