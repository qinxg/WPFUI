﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MyAssistant.Utility
{
    public class RegistryHelper
    {
        public static bool m_bCheckIsWow64 = true;

        public static string GetLocalMachineKeyValue(string keyPath, string propertyName)
        {
            return GetRegKey64(RegHive.HKEY_LOCAL_MACHINE, keyPath, GetRegSAM(), propertyName);
        }

        public static string GetLocalMachineKeyValueAll(string keyPath, string propertyName)
        {
            string v = string.Empty;
            v = GetLocalMachineKeyValue(keyPath, propertyName);

            if (string.IsNullOrEmpty(v) && GetRegSAM() == RegSAM.WOW64_64Key)
            {
                v = GetRegKey64(RegHive.HKEY_LOCAL_MACHINE, keyPath, RegSAM.WOW64_32Key, propertyName);
            }
            return v;
        }

        public static string GetLocalMachineKeyValue(string keyPath, string propertyName, RegSAM regSAM)
        {
            return GetRegKey64(RegHive.HKEY_LOCAL_MACHINE, keyPath, regSAM, propertyName);
        }

        public static string GeUsersKeyValue(string keyPath, string propertyName)
        {
            return GetRegKey64(RegHive.HKEY_USERS, keyPath, GetRegSAM(), propertyName);
        }

        public static List<string> GetSubKeyNames(string keyPath)
        {
            return GetSubKeyNames(keyPath, GetRegSAM());
        }

        private static RegSAM GetRegSAM()
        {
            //兼容64\32
            RegSAM s = RegSAM.WOW64_64Key;
            if (!InternalCheckIsWow64())
            {
                s = RegSAM.WOW64_32Key;
            }
            //SetupLogger.Info<RegistryHelper>("3264: " + s.ToString());
            return s;
        }

        public static bool InternalCheckIsWow64()
        {
            if (!m_bCheckIsWow64)
                return false;

            if ((Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1) ||
                Environment.OSVersion.Version.Major >= 6)
            {
                using (Process p = Process.GetCurrentProcess())
                {
                    bool retVal;
                    if (!IsWow64Process(p.Handle, out retVal))
                    {
                        return false;
                    }
                    return retVal;
                }
            }
            else
            {
                return false;
            }
        }

        public static bool IsExists(string keyPath, RegSAM regSAM)
        {
            UIntPtr hkey = UIntPtr.Zero;
            uint lResult = RegOpenKeyEx(RegHive.HKEY_LOCAL_MACHINE, keyPath, 0,
                    (int)RegSAM.Read | (int)regSAM, out hkey);
            return lResult == 0;
        }

        public static bool IsExists(string keyPath)
        {
            UIntPtr hkey = UIntPtr.Zero;
            uint lResult = RegOpenKeyEx(RegHive.HKEY_LOCAL_MACHINE, keyPath, 0,
                    (int)RegSAM.Read | (int)GetRegSAM(), out hkey);
            return lResult == 0;
        }

        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsWow64Process(
            [In] IntPtr hProcess,
            [Out] out bool wow64Process
        );

        [DllImport("Advapi32.dll")]
        static extern uint RegOpenKeyEx(
            UIntPtr hKey,
            string lpSubKey,
            uint ulOptions,
            int samDesired,
            out UIntPtr phkResult);

        [DllImport("Advapi32.dll")]
        static extern uint RegCloseKey(UIntPtr hkey);


        [DllImport("advapi32.dll", SetLastError = true)]
        public static extern uint RegQueryValueEx(
             UIntPtr hKey,
             string lpValueName,
             int lpReserved,
             ref RegistryValueKind lpType,
             IntPtr lpData,
             ref int lpcbData);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto)]
        internal static extern int RegQueryInfoKey(UIntPtr hkey, StringBuilder lpClass, int[] lpcbClass,
            IntPtr lpReserved_MustBeZero, ref int lpcSubKeys, int[] lpcbMaxSubKeyLen,
            int[] lpcbMaxClassLen, ref int lpcValues, int[] lpcbMaxValueNameLen,
            int[] lpcbMaxValueLen, int[] lpcbSecurityDescriptor, int[] lpftLastWriteTime);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto)]
        internal static extern int RegEnumKeyEx(UIntPtr hkey, int dwIndex, StringBuilder lpName,
            out int lpcbName, int[] lpReserved, StringBuilder lpClass, int[] lpcbClass, long[] lpftLastWriteTime);

        [DllImport("advapi32.dll", CharSet = CharSet.Auto)]
        internal static extern int RegEnumValue(UIntPtr hkey, int dwIndex, StringBuilder lpValueName,
            ref int lpcbValueName, IntPtr lpReserved_MustBeZero, int[] lpType, byte[] lpData, int[] lpcbData);

        [DllImport("advapi32.dll", CharSet = CharSet.Ansi)]
        internal static extern int RegEnumValueA(UIntPtr hkey, int dwIndex, StringBuilder lpValueName,
            ref int lpcbValueName, IntPtr lpReserved_MustBeZero, int[] lpType, byte[] lpData, int[] lpcbData);

        private static List<string> GetSubKeyNames(string keyName, RegSAM in32or64key)
        {
            UIntPtr hkey = UIntPtr.Zero;
            int lpcSubKeys = 0;
            int lpcValues = 0;
            uint lResult = RegOpenKeyEx(RegHive.HKEY_LOCAL_MACHINE, keyName, 0,
                    (int)RegSAM.Read | (int)in32or64key, out hkey);

            int errorCode = RegQueryInfoKey(hkey, null, null, IntPtr.Zero, ref lpcSubKeys, null, null, ref lpcValues, null, null, null, null);
            if (errorCode != 0)
            {
                throw new Exception("获取注册表键名列表时出错：" + errorCode.ToString() + keyName);
            }
            List<string> names = new List<string>();

            StringBuilder lpName = new StringBuilder(0x100);
            for (int i = 0; i < lpcSubKeys; i++)
            {
                int capacity = lpName.Capacity;
                int errorCode2 = RegEnumKeyEx(hkey, i, lpName, out capacity, null, null, null, null);
                if (errorCode2 != 0)
                {
                    throw new Exception("获取注册表键名列表时出错：" + errorCode2.ToString());
                }
                names.Add(lpName.ToString());
            }
            return names;
        }

        public static List<string> GetSubKeyPropertyNamesAll(string keyName)
        {
            List<string> ret = new List<string>();
            List<string> list32 = new List<string>();
            List<string> list64 = new List<string>();

            if (IsExists(keyName, RegSAM.WOW64_32Key))
            {
                list32 = GetSubKeyPropertyNames(keyName, RegSAM.WOW64_32Key);
            }
            if (IsExists(keyName, RegSAM.WOW64_64Key))
            {
                list64 = GetSubKeyPropertyNames(keyName, RegSAM.WOW64_64Key);
            }

            foreach (string s in list32)
            {
                if (!ret.Contains(s))
                {
                    ret.Add(s);
                }
            }

            foreach (string s2 in list64)
            {
                if (!ret.Contains(s2))
                {
                    ret.Add(s2);
                }
            }

            return ret;
        }

        public static List<string> GetSubKeyPropertyNames(string keyName)
        {
            return GetSubKeyPropertyNames(keyName, GetRegSAM());
        }

        public static List<string> GetSubKeyPropertyNames(string keyName, RegSAM regSAM)
        {
            RegSAM in32or64key = regSAM;
            UIntPtr hkey = UIntPtr.Zero;
            int lpcSubKeys = 0;
            int lpcValues = 0;
            uint lResult = RegOpenKeyEx(RegHive.HKEY_LOCAL_MACHINE, keyName, 0,
                    (int)RegSAM.Read | (int)in32or64key, out hkey);

            int errorCode = RegQueryInfoKey(hkey, null, null, IntPtr.Zero, ref lpcSubKeys, null, null, ref lpcValues, null, null, null, null);
            if (errorCode != 0)
            {
                throw new Exception("获取注册表键名列表时出错：" + errorCode.ToString() + keyName);
            }
            List<string> names = new List<string>();

            StringBuilder lpValueName = new StringBuilder(0x100);
            for (int i = 0; i < lpcValues; i++)
            {
                int capacity = lpValueName.Capacity;
                errorCode = RegEnumValue(hkey, i, lpValueName, ref capacity, IntPtr.Zero, null, null, null);
                if (errorCode == 0xea)
                {
                    int[] lpcbData = new int[1];
                    byte[] lpData = new byte[5];
                    lpcbData[0] = 5;
                    errorCode = RegEnumValueA(hkey, i, lpValueName, ref capacity, IntPtr.Zero, null, lpData, lpcbData);
                    if (errorCode == 0xea)
                    {
                        lpcbData[0] = 0;
                        errorCode = RegEnumValueA(hkey, i, lpValueName, ref capacity, IntPtr.Zero, null, null, lpcbData);
                    }
                }
                if (errorCode != 0)
                {
                    throw new Exception("获取注册表键子项列表时出错：" + errorCode.ToString());
                }
                names.Add(lpValueName.ToString());
            }
            return names;
        }

        private static string GetRegKey64(UIntPtr inHive, String inKeyName, String inPropertyName)
        {
            return GetRegKey64(inHive, inKeyName, RegSAM.WOW64_64Key, inPropertyName);
        }

        private static string GetRegKey32(UIntPtr inHive, String inKeyName, String inPropertyName)
        {
            return GetRegKey64(inHive, inKeyName, RegSAM.WOW64_32Key, inPropertyName);
        }

        private static string GetRegKey64(UIntPtr inHive, String inKeyName, RegSAM in32or64key, String inPropertyName)
        {
            //UIntPtr HKEY_LOCAL_MACHINE = (UIntPtr)0x80000002;
            UIntPtr hkey = UIntPtr.Zero;

            try
            {
                uint lResult = RegOpenKeyEx(inHive, inKeyName, 0,
                    (int)RegSAM.QueryValue | (int)in32or64key, out hkey);

                if (0 != lResult) return null;

                IntPtr pResult = IntPtr.Zero;
                int size = 0;
                RegistryValueKind type = RegistryValueKind.Unknown;

                uint retVal = RegQueryValueEx(hkey, inPropertyName, 0, ref type, IntPtr.Zero, ref size);
                if (size == 0)
                {
                    return null;
                }

                pResult = Marshal.AllocHGlobal(size);

                retVal = RegQueryValueEx(hkey, inPropertyName, 0, ref type, pResult, ref size);
                if (retVal != 0)
                {
                    return null;
                }
                else
                {
                    switch (type)
                    {
                        case RegistryValueKind.String:
                            return Marshal.PtrToStringAnsi(pResult);
                        case RegistryValueKind.DWord:
                            return Convert.ToString(Marshal.ReadInt32(pResult));
                        case RegistryValueKind.QWord:
                            return Convert.ToString(Marshal.ReadInt64(pResult));
                    }
                }
                return null;

            }
            finally
            {
                if (UIntPtr.Zero != hkey) RegCloseKey(hkey);
            }
        }
    }

    public enum RegSAM
    {
        QueryValue = 0x0001,
        SetValue = 0x0002,
        CreateSubKey = 0x0004,
        EnumerateSubKeys = 0x0008,
        Notify = 0x0010,
        CreateLink = 0x0020,
        WOW64_32Key = 0x0200,
        WOW64_64Key = 0x0100,
        WOW64_Res = 0x0300,
        Read = 0x00020019,
        Write = 0x00020006,
        Execute = 0x00020019,
        AllAccess = 0x000f003f
    }

    public static class RegHive
    {
        public static UIntPtr HKEY_LOCAL_MACHINE = new UIntPtr(0x80000002u);
        public static UIntPtr HKEY_CURRENT_USER = new UIntPtr(0x80000001u);
        public static UIntPtr HKEY_USERS = new UIntPtr(0x80000003u);
    }
}
