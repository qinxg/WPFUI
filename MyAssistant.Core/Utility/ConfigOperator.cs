﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace MyAssistant.Utility
{
    public  static class ConfigOperator
    {
        public static void Write(string path)
        {
            var config = Path.Combine(path, "Web.config");
            XmlDocument xml = new XmlDocument();
            xml.Load(config);

            var webModule = xml.SelectSingleNode("/configuration/system.web/httpModules/add[@name='IntegrationModule']");

            if (webModule != null)
                webModule.Attributes["type"].Value =
                    "Mysoft.Integration.HttpExtensions.IntegrationHttpModule, Mysoft.Integration.HttpExtensions";
            else
            {

                var web = xml.SelectSingleNode("/configuration/system.web");

                var parent = xml.SelectSingleNode("/configuration/system.web/httpModules");

                if (parent == null)
                {
                    parent = xml.CreateElement("httpModules");
                    web.AppendChild(parent);
                }

                var xmlElement = xml.CreateElement("add");
                xmlElement.SetAttribute("name", "IntegrationModule");
                xmlElement.SetAttribute("type", "Mysoft.Integration.HttpExtensions.IntegrationHttpModule, Mysoft.Integration.HttpExtensions");
                parent.AppendChild(xmlElement);
            }



            var webServerModule = xml.SelectSingleNode("/configuration/system.webServer/modules/add[@name='IntegrationModule']");

            if (webServerModule != null)
                webServerModule.Attributes["type"].Value =
                    "Mysoft.Integration.HttpExtensions.IntegrationHttpModule, Mysoft.Integration.HttpExtensions";

            else
            {
                var web = xml.SelectSingleNode("/configuration/system.webServer");
                var parent = xml.SelectSingleNode("/configuration/system.webServer/modules");
                if (parent == null)
                {
                    parent = xml.CreateElement("modules");
                    web.AppendChild(parent);
                }

                var xmlElement = xml.CreateElement("add");
                xmlElement.SetAttribute("name", "IntegrationModule");
                xmlElement.SetAttribute("type",
                    "Mysoft.Integration.HttpExtensions.IntegrationHttpModule, Mysoft.Integration.HttpExtensions");
                xmlElement.SetAttribute("preCondition", "integratedMode");
                parent.AppendChild(xmlElement);
            }

            xml.Save(config);

        }
    }
}
