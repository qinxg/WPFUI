﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using ReactiveUI;

namespace MyAssistant.Utility
{
    public sealed class CacheResult<T>
    {
        private Exception _exception;
        private T _result;

        /// <summary>
        /// 缓存结果项
        /// 
        /// </summary>
        public T Result
        {
            get
            {
                if (this._exception != null)
                    throw this._exception;
                return this._result;
            }
        }

        internal CacheResult(T result, Exception ex)
        {
            this._exception = ex;
            this._result = result;
        }
    }

    public sealed class FileDependencyManager<T>
    {
        /// <summary>
        /// 等待文件句柄关闭的时间，单位：毫秒，默认值：3000（3秒）。
        ///             默认值是一个比较保守的时间，为了快速运行单元测试用例，可以修改这个时间
        /// 
        /// </summary>
        private static int s_WaitFileCloseTimeout = 3000;
        private readonly string CacheKey = Guid.NewGuid().ToString();
        private string[] _files;
        private Func<string[], T> _func;
        private CacheResult<T> _cacheResult;

        /// <summary>
        /// 缓存结果
        /// 
        /// </summary>
        public T Result => this._cacheResult.Result;

        /// <summary>
        /// 构造方法
        /// 
        /// </summary>
        /// <param name="func"/><param name="files"/>
        public FileDependencyManager(Func<string[], T> func, params string[] files)
        {
            if (func == null)
                throw new ArgumentNullException(nameof(func));
            if (files == null || files.Length == 0)
                throw new ArgumentNullException(nameof(files));
            this._func = func;
            this._files = files;
            this.GetObject();
        }

        private void GetObject()
        {
            Exception ex1 = (Exception) null;
            T result = default(T);
            try
            {
                result = this._func(this._files);
            }
            catch (Exception ex2)
            {
                ex1 = ex2;
            }

            ObjectCache c = MemoryCache.Default;
            object content = c[this.CacheKey];

            if (content == null)
            {
                CacheItemPolicy p = new CacheItemPolicy();
                p.ChangeMonitors.Add(new HostFileChangeMonitor(_files));
                p.UpdateCallback = args =>
                {
                    Thread.Sleep(FileDependencyManager<T>.s_WaitFileCloseTimeout);
                    this.GetObject();
                };
                c.Set(this.CacheKey, result, p);
            }

            this._cacheResult = new CacheResult<T>(result, ex1);
        }
    }
}
