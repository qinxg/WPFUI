﻿using System.IO;
using System.Xml;

namespace MyAssistant.Business
{
    public class ConfigBusiness
    {
        public void SetConfig(string sitePath)
        {
            var config = Path.Combine(sitePath, "Web.config");
            var xml = new XmlDocument();
            xml.Load(config);

            var webModule = xml.SelectSingleNode("/configuration/system.web/httpModules/add[@name='IntegrationModule']");

            if (webModule != null)
                webModule.Attributes["type"].Value =
                    "Mysoft.Integration.HttpExtensions.IntegrationHttpModule, Mysoft.Integration.HttpExtensions";
            else
            {
                var web = xml.SelectSingleNode("/configuration/system.web");

                var parent = xml.SelectSingleNode("/configuration/system.web/httpModules");

                if (parent == null)
                {
                    parent = xml.CreateElement("httpModules");
                    web.AppendChild(parent);
                }

                var xmlElement = xml.CreateElement("add");
                xmlElement.SetAttribute("name", "IntegrationModule");
                xmlElement.SetAttribute("type",
                    "Mysoft.Integration.HttpExtensions.IntegrationHttpModule, Mysoft.Integration.HttpExtensions");
                parent.AppendChild(xmlElement);
            }


            var webServerModule =
                xml.SelectSingleNode("/configuration/system.webServer/modules/add[@name='IntegrationModule']");

            if (webServerModule != null)
                webServerModule.Attributes["type"].Value =
                    "Mysoft.Integration.HttpExtensions.IntegrationHttpModule, Mysoft.Integration.HttpExtensions";

            else
            {
                var web = xml.SelectSingleNode("/configuration/system.webServer");
                var parent = xml.SelectSingleNode("/configuration/system.webServer/modules");
                if (parent == null)
                {
                    parent = xml.CreateElement("modules");
                    web.AppendChild(parent);
                }

                var xmlElement = xml.CreateElement("add");
                xmlElement.SetAttribute("name", "IntegrationModule");
                xmlElement.SetAttribute("type",
                    "Mysoft.Integration.HttpExtensions.IntegrationHttpModule, Mysoft.Integration.HttpExtensions");
                xmlElement.SetAttribute("preCondition", "integratedMode");
                parent.AppendChild(xmlElement);
            }

            xml.Save(config);
        }
    }
}
