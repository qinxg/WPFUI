﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using FSLib.Network.Http;
using MyAssistant.Management;
using MyAssistant.Model;
using MyAssistant.Services;
using MyAssistant.Utility;
using Newtonsoft.Json;
using Splat;

namespace MyAssistant.Plugin
{
    /// <summary>
    ///     插件基类
    /// </summary>
    public abstract class PluginBase : IEnableLogger
    {
        protected readonly IDirectoryIISService _iisService;

        protected readonly ILibraryService _libraryService;


        /// <summary>
        ///     初始化
        /// </summary>
        /// <param name="libraryService"></param>
        /// <param name="iisService"></param>
        protected PluginBase(ILibraryService libraryService, IDirectoryIISService iisService)
        {
            _libraryService = libraryService;
            _iisService = iisService;

            var classAttribute = GetType().GetCustomAttribute<PluginClassAttribute>(true);

            if (classAttribute == null)
                throw new KeyNotFoundException($"{GetType().Name}不包含{nameof(PluginClassAttribute)}属性");
        }

        /// <summary>
        ///     站点地址过滤表达式
        /// </summary>
        public virtual Func<SiteAttribute, bool> SiteMatch { get; } = p => p.Name == "站点地址";


        public PluginContext Context { get; set; }


        /// <summary>
        ///     获取初始化数据
        /// </summary>
        /// <returns></returns>
        public virtual List<InitData> GetInitData(List<SiteInfo> sites)
        {
            var result = new List<InitData>();

            foreach (var site in sites)
            {
                var data = new InitData
                {
                    IsMe = site.Title == Context.Title,
                    IsMain = site.IsMain,
                    Id = site.Id,
                    Name = site.Title,
                    Url = site.Data.FirstOrDefault(SiteMatch)?.Value,
                    Type = site.Type,
                    DBInfo = new InitDBInfo
                    {
                        ServerName = site.Data.FirstOrDefault(p => p.Name == "DB服务器名")?.Value,
                        UserName = site.Data.FirstOrDefault(p => p.Name == "用户名")?.Value,
                        Password = site.Data.FirstOrDefault(p => p.Name == "密码")?.Value,
                        DBName = site.Data.FirstOrDefault(p => p.Name == "数据库名")?.Value
                    }
                };

                result.Add(data);
            }
            return result;
        }



        #region 初始化
        public HttpStatus Init()
        {
            //处理站点非必填场景。
            if ((Context.CurrentSite.Required == false) && string.IsNullOrEmpty(Context.Url))
                return new HttpStatus { Succes = true };

            BeforeInit();

            var result = InitSite();

            AfterInit();

            return result;
        }


        public virtual void BeforeInit()
        {
            
        }


        /// <summary>
        ///     初始化
        /// </summary>
        /// <returns></returns>
        public virtual HttpStatus InitSite()
        {
            var client = new HttpClient();
            client.Setting.DefaultRequestContentType = ContentType.Json;

            var success = true;
            var msg = "";

            var data = GetInitData(Context.Library.Sites);

            try
            {
                var context =
                    client.Post<string>($"{Context.Url}{Context.CurrentSite.InitRelativePath}", data, null, true, false).Send();

                if (!context.IsFinished)
                {
                    success = false;
                    msg = context.Exception.Message;
                }
                else
                {
                    success = context.IsSuccess;
                    if (success)
                    {
                        if ((context.Status != HttpStatusCode.OK) && (context.Status != HttpStatusCode.NoContent))
                        {
                            success = false;
                            msg = $"初始化失败，请检查站点地址{Context.Url}是否正确，站点是否可以正常打开。\r\n";
                        }
                        else
                            msg = context.Result;
                    }
                    else
                    {
                        success = false;
                        msg =
                            $"{Context.Url}站点地址配置错误，请检查站点配置！\r\n" +
                            (context.Exception == null
                                ? context.Result
                                : context.Exception?.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log().FatalException(ex.Message, ex);
                throw new Exception($"{Context.Url}站点地址配置错误，请修改！");
            }


            return new HttpStatus
            {
                Message = msg,
                Succes = success
            };
        }


        public virtual void AfterInit()
        {
            
        }

        #endregion
        


        /// <summary>
        ///     获取检查项
        /// </summary>
        /// <returns></returns>
        public abstract List<InspectionItem> GetInspectionItems();


        public virtual InspectionResult GetDynamicInspectionResult(string id)
        {
            return null;
        }

        /// <summary>
        ///     检查前操作
        /// </summary>
        public virtual void BeforeInspect()
        {
        }



        public virtual bool CheckConfig()
        {
            var client = new HttpClient();
            var context =
                client.Get<string>($"{Context.Url}{Context.CurrentSite.InitInspectRelativePath}").Send();
            if (context.IsSuccess && 
                context.Response.ContentType.StartsWith("application/json;", StringComparison.InvariantCultureIgnoreCase))
            {
                return true;
            }
            return false;
        }
    }


    /// <summary>
    ///     初始化数据
    /// </summary>
    public class InitData
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("isMe")]
        public bool IsMe { get; set; }

        [JsonProperty("isMain")]
        public bool IsMain { get; set; }


        //TOOD:改成枚举
        [JsonProperty("type")]
        public string Type { get; set; }


        [JsonProperty("dbInfo")]
        public InitDBInfo DBInfo { get; set; }
    }


    /// <summary>
    ///     初始化数据库信息
    /// </summary>
    public class InitDBInfo
    {
        [JsonProperty("serverName")]
        public string ServerName { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }


        [JsonProperty("dbName")]
        public string DBName { get; set; }
    }


    public class InitDataResult
    {
        public bool Enable { get; set; }
        public List<InitData> Sites { get; set; }
    }

    public class SiteInspectResult
    {
        public string Address { get; set; }
        public InitDataResult SSO { get; set; }
        public InitDataResult MIP { get; set; }
        public InitDataResult WF { get; set; }
    }

}