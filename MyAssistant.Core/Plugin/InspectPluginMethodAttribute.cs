﻿using System;

namespace MyAssistant.Plugin
{
    [AttributeUsage(AttributeTargets.Method)]
    public class InspectPluginMethodAttribute : Attribute
    {
        /// <summary>
        /// 描述信息
        /// </summary>
        public  string Id { get;  set; }


        public  int Order { get; set; }
    }
}