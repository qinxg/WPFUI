﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

namespace MyAssistant.Plugin
{
    class PluginUtilities
    {
        public static PluginBase Resolve(IComponentContext context, string className)
        {
            return context.ResolveNamed<PluginBase>(className);
        }
    }
}
