﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAssistant.Plugin
{
    [AttributeUsage(AttributeTargets.Class)]
    public class PluginClassAttribute : Attribute
    { 
        //public PluginClassAttribute(string type ,string initRelativePath = null)
        //{
        //    this.Type = type;
        //    this.InitRelativePath = initRelativePath;
        //}


        public  string Type { get; set; }

        /// <summary>
        /// 初始化相对路径
        /// </summary>
        public string InitRelativePath { get;  set; }
    }
}
