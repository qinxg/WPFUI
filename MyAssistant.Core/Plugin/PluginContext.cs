﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyAssistant.Management;
using MyAssistant.Model;

namespace MyAssistant.Plugin
{
    /// <summary>
    /// 插件上下文
    /// </summary>
    public class PluginContext
    {
        /// <summary>
        /// 当前站点需要发起请求的Url
        /// </summary>
        public string Url { get; set; }

        public Library Library { get; set; }

        /// <summary>
        /// 站点标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 当前站点
        /// </summary>
        public SiteInfo CurrentSite => Library.Sites.FirstOrDefault(p => p.Title == Title);


        /// <summary>
        /// 站点IIS名称
        /// </summary>
        /// <returns></returns>
        public string SiteIISName => CurrentSite.Data.FirstOrDefault(p => p.Name == "站点位置")?.Value;

        /// <summary>
        /// 主站点IIS名称
        /// </summary>
        /// <returns></returns>
        public string MainSiteIISName
            => Library.Sites.FirstOrDefault(p => p.IsMain)?.Data.FirstOrDefault(p => p.Name == "站点位置")?.Value;

    }
}
