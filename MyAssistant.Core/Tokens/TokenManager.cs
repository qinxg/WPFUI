﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyAssistant.Management;
using MyAssistant.Model;

namespace MyAssistant.Tokens
{
    public class TokenManager : ITokenManager
    {
        //private readonly IEnumerable<ITokenProvider> _providers;

        //public TokenManager(IEnumerable<ITokenProvider> providers)
        //{
        //    _providers = providers;
        //}

        //public IEnumerable<TokenTypeDescriptor> Describe(IEnumerable<string> targets)
        //{
        //    var context = new DescribeContextImpl();
        //    foreach (var provider in _providers)
        //    {
        //        provider.Describe(context);
        //    }
        //    return context.Describe((targets ?? Enumerable.Empty<string>()).ToArray());
        //}

        public IDictionary<string, object> Evaluate(string target, IDictionary<string, string> tokens, Library data)
        {
            //var context = new EvaluateContextImpl(target, tokens, data, this);
            //foreach (var provider in _providers)
            //{
            //    provider.Evaluate(context);
            //}
            //return context.Produce();


            var dict = new Dictionary<string, object>();

            var site = data.Sites.FirstOrDefault(p => p.Type == target);

            if (site == null) return dict;

            foreach (var token in tokens)
            {
                var val = site.Data.FirstOrDefault(p => p.Name == token.Key);
                if (val != null)
                {
                    dict.Add(token.Value, val.Value);
                    continue;
                }

                var field = site.GetType().GetField(token.Key);

                if (field != null)
                {
                    var rtn = field.GetValue(site);
                    if (rtn != null)
                        dict.Add(token.Value, (string) rtn);
                }
            }

            return dict;
        }

    }
}
