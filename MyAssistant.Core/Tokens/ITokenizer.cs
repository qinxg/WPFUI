﻿using System.Collections.Generic;
using MyAssistant.Management;

namespace MyAssistant.Tokens
{
    public interface ITokenizer 
    {
        string Replace(string text, Library data);
    }
}
