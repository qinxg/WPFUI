﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyAssistant.Management;

namespace MyAssistant.Tokens
{
    public interface ITokenManager 
    {
        //IEnumerable<TokenTypeDescriptor> Describe(IEnumerable<string> targets);
        IDictionary<string, object> Evaluate(string target, IDictionary<string, string> tokens, Library data);
    }
}
