﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace MyAssistant.Core
{
    public  static class AppInfo
    {
        public static readonly string BlobCachePath;
        public static readonly string LibraryFilePath;
        public static readonly string LogFilePath;
        public static readonly string OverridenApplicationDataPath;
        public static readonly Version Version;
        public static readonly string UpdatePath;
        public static readonly string ApplicationRootPath;
        public static readonly string MIPInitPath;
        public static readonly string RunPath;




        static AppInfo()
        {
//#if DEBUG
            //调试模式时，数据写入在
            OverridenApplicationDataPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "MysoftDebug");
            //#endif
            RunPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "..");
            ApplicationRootPath = OverridenApplicationDataPath ?? RunPath;
            MIPInitPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "data", "mip");
            BlobCachePath = Path.Combine(ApplicationRootPath, "BlobCache");
            LibraryFilePath = Path.Combine(ApplicationRootPath, "Library.json");
            LogFilePath = Path.Combine(ApplicationRootPath, "Log.txt");
            UpdatePath = "http://";
            Version = Assembly.GetExecutingAssembly().GetName().Version;
        }

    }
}
