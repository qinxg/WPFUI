﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAssistant.Model
{

    /// <summary>
    /// IIS站点基本信息
    /// </summary>
    public class IISSite
    {
        /// <summary>
        /// 站点标识
        /// </summary>
        public string SiteID { set; get; }

        /// <summary>
        /// 站点名称
        /// </summary>
        public string SiteName { set; get; }

        /// <summary>
        /// 站点路径
        /// </summary>
        public string SitePath { set; get; }

        /// <summary>
        /// 站点端口
        /// </summary>
        public List<string> SitePorts { set; get; }
    }


    /// <summary>
    /// IIS虚拟目录基本信息
    /// </summary>
    public class IISVirtualDirectory {

        /// <summary>
        /// 当前站点标识
        /// </summary>
        public string SiteID { set; get; }


        /// <summary>
        /// 虚拟目录名称
        /// </summary>
        public string VirtualDirectoryName {
            set;get;
        }

        /// <summary>
        /// 虚拟目录路径
        /// </summary>
        public string VirtualDirectoryPath { set; get; }
    }
}
