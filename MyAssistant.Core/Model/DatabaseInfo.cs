﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAssistant.Model
{
    public class DatabaseInfo
    {
        public string Server { get; set; }
        public string Password { get; set; }
        public string User { get; set; }

        public string InitialCatalog { get; set; }
    }
}
