﻿using System;
using Newtonsoft.Json;
using ReactiveUI;

namespace MyAssistant.Model
{
    [Serializable]
    public class SiteAttribute : ReactiveObject
    {
        [JsonProperty("name")]
        public string Name { get; set; }


        private string _value;

        [JsonProperty("value")]
        public string Value
        {
            get { return this._value; }
            set { this.RaiseAndSetIfChanged(ref this._value, value);  }
        }


        private bool? _display;

        [JsonProperty("display")]
        public bool? Display
        {
            get { return this._display; }
            set { this.RaiseAndSetIfChanged(ref this._display, value); }
        }
    }
}