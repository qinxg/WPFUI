﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;
using ReactiveUI;

namespace MyAssistant.Model
{
    /// <summary>
    ///     检测项详情
    /// </summary>
    public class InspectionResult
    {
        /// <summary>
        ///     结果
        /// </summary>
        public EnumStatus Status { get; set; }

        /// <summary>
        ///     帮助
        /// </summary>
        public string[] Help { get; set; }


        /// <summary>
        ///     显示帮助
        /// </summary>
        public bool DisplayHelp => Status == EnumStatus.Fail;
    }


    public enum EnumStatus
    {
        [Description("未开始")] None = 0,
        [Description("执行中")] Doing = 1,
        [Description("成功")] Success = 2,
        [Description("失败")] Fail = 3
    }

    /// <summary>
    ///     检查项
    /// </summary>
    [Serializable]
    public class InspectionItem : ReactiveObject
    {

        private InspectionResult _result;


        public InspectionItem()
        {
            Result = new InspectionResult()
            {
                Status = 0
            };
        }

        [JsonProperty("ID")]
        public string Id { get; set; }

        [JsonProperty("Description")]
        public string Description { get; set; }

        public  InspectionResult  Result
        {
            get { return _result; }

            set { this.RaiseAndSetIfChanged(ref _result, value); }
        }
    }
}