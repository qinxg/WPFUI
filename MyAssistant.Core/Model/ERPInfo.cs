﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAssistant.Model
{
    public class ERPInfo
    {
        public string ERPSite { get; set; }
        public string DBAddress { get; set; }
        public string DBName { get; set; }
        public string DBUser { get; set; }
        public string DBPwd { get; set; }
    }
}
