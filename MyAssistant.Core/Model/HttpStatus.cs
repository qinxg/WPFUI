﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyAssistant.Model
{
    /// <summary>
    /// http状态
    /// </summary>
    public class HttpStatus
    {
        public bool Succes { get; set; }

        public  string Message { get; set; }
    }
}
