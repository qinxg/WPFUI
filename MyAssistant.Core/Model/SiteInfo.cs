﻿using System;
using MyAssistant.Model;
using Newtonsoft.Json;
using ReactiveUI;

namespace MyAssistant.Model
{
    
    [Serializable]
    public class SiteInfo
    {


        /// <summary>
        /// 站点标题
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }


        /// <summary>
        /// 站点标识
        /// </summary>
        [JsonProperty("id")]
        public  Guid Id { get; set; }


        /// <summary>
        /// 必填
        /// </summary>
        [JsonProperty("required")]
        public bool Required { get; set; }


        /// <summary>
        /// 是否选中
        /// </summary>
        [JsonProperty("selected")]
        public bool Selected { get; set; }

        /// <summary>
        /// 站点数据
        /// </summary>
        [JsonProperty("data")]
        public ReactiveList<SiteAttribute> Data { get; set; }


        /// <summary>
        /// 是否为主站点
        /// </summary>
        [JsonProperty("isMain")]
        public bool IsMain { get; set; }


        /// <summary>
        /// 站点类型
        /// </summary>
        [JsonProperty("type")]
        public  string Type { get; set; }


        /// <summary>
        /// 业务逻辑类
        /// </summary>
        [JsonProperty("businessClass")]
        public string BusinessClass { get; set; }

        /// <summary>
        /// 初始化信息地址
        /// </summary>
        [JsonProperty("initRelativePath")]
        public  string InitRelativePath { get; set; }


        /// <summary>
        /// 初始化检查地址
        /// </summary>
        [JsonProperty("initInspectRelativePath")]
        public string InitInspectRelativePath { get; set; }

        /// <summary>
        /// 顺序
        /// </summary>
        [JsonProperty("order")]
        public int Order { get; set; }
    }
}
