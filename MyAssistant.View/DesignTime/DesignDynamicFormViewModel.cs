﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyAssistant.Model;
using MyAssistant.ViewModels;
using MyAssistant.Views;
using ReactiveUI;

namespace MyAssistant.DesignTime
{
    public class DesignDynamicFormViewModel : DynamicFormViewModel
    {
        public DesignDynamicFormViewModel() : base(new SiteInfo
        {
            Data = new ReactiveList<SiteAttribute>
            {
                new SiteAttribute {Name = "站点地址", Value = "http://"},
                new SiteAttribute {Name = "DB服务器名", Value = ""},
                new SiteAttribute {Name = "用户名", Value = ""},
                new SiteAttribute {Name = "密码", Value = ""},
                new SiteAttribute {Name = "数据库名", Value = ""}

            },
            Title = "预览数据"
        },null,null,null,null,null)
        {

        }
    }
}
