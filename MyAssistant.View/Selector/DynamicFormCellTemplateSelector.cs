﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MyAssistant.Model;

namespace MyAssistant.Selector
{
    public class DynamicFormCellTemplateSelector : DataTemplateSelector
    {

        /// <summary>
        /// 文本框模板
        /// </summary>
        public DataTemplate TextFiledDataTemplate { get; set; }

        /// <summary>
        /// 密码模板
        /// </summary>
        public DataTemplate PasswordFiledDataTemplate { get; set; }


        /// <summary>
        /// 数据库字段模板
        /// </summary>
        public DataTemplate DatabaseFiledDataTemplate { get; set; }


        public  DataTemplate IISFiledDataTemplate { get; set; }
        
        /// <summary>
        /// 负载均衡站点
        /// </summary>
        public  DataTemplate LoadBalanceFieldDataTemplate { get; set; }




        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            var attr = item as SiteAttribute;

            if (attr != null)
            {
                if (attr.Name == "密码")
                    return PasswordFiledDataTemplate;

                if (attr.Name == "数据库名")
                    return DatabaseFiledDataTemplate;

                if (attr.Name == "站点位置")
                    return IISFiledDataTemplate;

                if (attr.Name.StartsWith("内网地址"))
                    return LoadBalanceFieldDataTemplate;
            }
            return TextFiledDataTemplate;
        }
    }
}
