﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MyAssistant.ViewModels;
using ReactiveUI;

namespace MyAssistant.Views
{
    /// <summary>
    /// InspectView.xaml 的交互逻辑
    /// </summary>
    public partial class InspectView : IViewFor<InspectViewModel>
    {
        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel", typeof(InspectViewModel), typeof(InspectView));


        public InspectView()
        {
            InitializeComponent();
            this.Events().DataContextChanged.Subscribe(args =>
             this.ViewModel = (InspectViewModel)args.NewValue);
        }

        object IViewFor.ViewModel
        {
            get { return ViewModel; }
            set { ViewModel = (InspectViewModel) value; }
        }

        public InspectViewModel ViewModel
        {
            get { return (InspectViewModel)this.GetValue(ViewModelProperty); }
            set { this.SetValue(ViewModelProperty, value); }
        }
    }
}
