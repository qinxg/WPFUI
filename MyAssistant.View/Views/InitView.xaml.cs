﻿using System;
using System.Reactive.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using MyAssistant.Model;
using MyAssistant.ViewModels;
using ReactiveUI;

namespace MyAssistant.Views
{
    /// <summary>
    /// SettingsView.xaml 的交互逻辑
    /// </summary>
    public partial class InitView :  IViewFor<InitViewModel>
    {

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel", typeof(InitViewModel), typeof(InitView));


        public InitView()
        {
            InitializeComponent();

            this.Events().DataContextChanged.Subscribe(args =>
                {
                    this.ViewModel = (InitViewModel) args.NewValue;
                }
            );

        }

        object IViewFor.ViewModel
        {
            get { return ViewModel; }
            set { ViewModel = (InitViewModel)value; }
        }

        public InitViewModel ViewModel
        {
            get { return (InitViewModel)this.GetValue(ViewModelProperty); }
            set { this.SetValue(ViewModelProperty, value); }
        }
    }
}
