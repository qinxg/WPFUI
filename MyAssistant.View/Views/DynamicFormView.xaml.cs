﻿using System;
using System.Reactive.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using MyAssistant.ViewModels;
using ReactiveUI;
using ReactiveUI.Legacy;

namespace MyAssistant.Views
{
    /// <summary>
    /// DynamicForm.xaml 的交互逻辑
    /// </summary>
    public partial class DynamicFormView :  IViewFor<DynamicFormViewModel>
    {

        public static readonly DependencyProperty ViewModelProperty = DependencyProperty.Register("ViewModel", typeof(DynamicFormViewModel), typeof(DynamicFormView));


        public DynamicFormView()
        {
            InitializeComponent();

            this.Events().DataContextChanged.Subscribe(args =>
                    this.ViewModel = (DynamicFormViewModel) args.NewValue);

        }

        object IViewFor.ViewModel
        {
            get { return ViewModel; }
            set { ViewModel = (DynamicFormViewModel)value; }
        }

        public DynamicFormViewModel ViewModel
        {
            get { return (DynamicFormViewModel)this.GetValue(ViewModelProperty); }
            set { this.SetValue(ViewModelProperty, value); }
        }


    }
}
