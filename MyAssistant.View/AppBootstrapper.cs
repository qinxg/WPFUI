﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

using Autofac;
using Caliburn.Micro;
using FSLib.App.SimpleUpdater;
using IniParser;
using IniParser.Model;
using MyAssistant.Core;
using MyAssistant.Plugin;
using MyAssistant.Services;
using MyAssistant.ViewModels;
using MyAssistant.Views;
using NLog.Config;
using NLog.Targets;
using Splat;
using LogLevel = NLog.LogLevel;
using LogManager = NLog.LogManager;
using MyAssistant.Common.Inspect.Plugin;
using MyAssistant.Common.Inspect.Plugin.ERP;
using MyAssistant.Management;

namespace MyAssistant
{
    internal class AppBootstrapper : AutofacBootstrapper<ShellViewModel>, IEnableLogger
    {

        public AppBootstrapper()
        {
            Initialize();
        }

        protected override void Configure()
        {
            base.Configure();
            ConfigureLogging();
        }

        protected override void ConfigureContainer(ContainerBuilder builder)
        {
            base.ConfigureContainer(builder);

            //  register Services
            builder.RegisterAssemblyTypes(AssemblySource.Instance.ToArray())
                .Where(type => type.Name.EndsWith("Service"))
                .Where(type =>
                        (string.IsNullOrWhiteSpace(type.Namespace) == false) && type.Namespace.EndsWith("Services"))
                .AsImplementedInterfaces()
                .InstancePerDependency();

            
            //注册插件
            builder.RegisterAssemblyTypes(typeof(ERPInspectBase).Assembly)
                .Where(p => p.IsSubclassOf(typeof(PluginBase)))
                .Named<PluginBase>(p => p.Name);
        }

        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            var assemblies = base.SelectAssemblies().ToList();
            assemblies.Add(typeof(ShellViewModel).Assembly);
            return assemblies;
        }


        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            Update();

            Directory.CreateDirectory(AppInfo.ApplicationRootPath);

            var service = Container.Resolve<ILibraryService>();
            var library = service.Get();

            if (library.Sites == null)
            {
                library.Sites = service.GetDefaultSites(EnumMode.SignleServer);
                service.Set(library);
            }

            DisplayRootViewFor<ShellViewModel>();
        }



        /// <summary>
        /// 应用更新
        /// </summary>
        private void Update()
        {

            var parser = new FileIniDataParser();
            IniData data = parser.ReadFile("assistant.ini");


            string url = data["update"]["url"];
            string type = data["update"]["type"];

            var path = type == "0" ? "debug" : "release";

            var updateUrl = $"{url}/{path}/{{0}}";

            Task.Run(() =>
                    Updater.CheckUpdateSimple(updateUrl, "update_c.xml"));


        }


        private void ConfigureLogging()
        {

            //todo:这里晚些换为autofac
            var logConfig = new LoggingConfiguration();

            var target = new FileTarget
            {
                FileName = AppInfo.LogFilePath,
                Layout = @"${longdate}|${level}|${message} ${exception:format=ToString,StackTrace}",
                ArchiveAboveSize = 1024*1024*2, // 2 MB
                ArchiveNumbering = ArchiveNumberingMode.Sequence
            };

            logConfig.LoggingRules.Add(new LoggingRule("*", LogLevel.Info, target));
            LogManager.Configuration = logConfig;
            Locator.CurrentMutable.RegisterConstant(new NLogLogger(LogManager.GetCurrentClassLogger()), typeof(ILogger));
        }
    }
}