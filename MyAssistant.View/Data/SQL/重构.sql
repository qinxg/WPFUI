﻿IF NOT EXISTS ( SELECT 1 FROM sys.servers WHERE name = '{核心.DB服务器名}' )
BEGIN
	EXEC master.dbo.sp_addlinkedserver 
	@server = N'{核心.DB服务器名}', @srvproduct=N'SQL Server' 
	EXEC master.dbo.sp_addlinkedsrvlogin 
	@rmtsrvname=N'{核心.DB服务器名}',@useself=N'False',@locallogin=N'{核心.用户名}',@rmtuser=N'{核心.用户名}',@rmtpassword='{核心.密码}'
END

--初始化桌面部件脚本
DECLARE @cgDB NVARCHAR(200)
DECLARE @hxDB NVARCHAR(200)
DECLARE @sql NVARCHAR(max)

/***********************  在执行前请先修改数据库  ***********************************/
SET @cgDB='{重构.数据库名}'     --重构数据库
SET @hxDB='{核心.DB服务器名}].[{核心.数据库名}'     --ERP352数据库
/************************************************************************************/


SET @sql='DELETE ['+@cgDB+'].dbo.p_DesktopPart WHERE  Application LIKE ''%_351''

insert into ['+@cgDB+'].dbo.p_DesktopPart 
([p_DesktopPartId],[CreatedTime],[CreatedGUID],[ModifiedTime],[ModifiedGUID],
[PartUrl],[PartContent],[Application],[PartName],[PartViewUrl],[Height],[IsSys]) 
select WPGUID as [p_DesktopPartId],CreatedOn  as [CreatedTime],CreatedBy as [CreatedGUID],ModifiedOn as [ModifiedTime],ModifiedBy as [ModifiedGUID],
URL as [PartUrl],Comments as [PartContent], Application+''_351'' as [Application] ,WPName as  [PartName], PartViewUrl as   [PartViewUrl],Height as [Height],IsSys as [IsSys]
  from ['+@hxDB+'].dbo.myWebpart  where WPType<>''0000010001'' and Application not in(''0000'',''1001'') '
    
  exec (@sql)
  
 SET @sql='Update ['+@cgDB+'].dbo.p_DesktopPart  set  Height=''510'' WHERE  PartName = ''采招公示'' '
    
  exec (@sql)


--01、清除垃圾数据
SELECT @sql = N'
delete ['+@cgDB+'].dbo.myWorkflowStepModuleDefinition where ProcessDefinitionGUID not in (SELECT ProcessDefinitionGUID FROM ['+@cgDB+'].dbo.myWorkflowProcessModuleDefinition)
delete ['+@cgDB+'].dbo.myWorkflowBandModuleDefinition where ProcessDefinitionGUID not in (SELECT ProcessDefinitionGUID FROM ['+@cgDB+'].dbo.myWorkflowProcessModuleDefinition)
delete ['+@cgDB+'].dbo.myWorkflowBandModuleDefinition where ProcessDefinitionGUID not in (SELECT ProcessDefinitionGUID FROM ['+@cgDB+'].dbo.myWorkflowProcessModuleDefinition)
delete ['+@cgDB+'].dbo.myWorkflowCCModuleConditionDefinition where ProcessDefinitionGUID not in (SELECT ProcessDefinitionGUID FROM ['+@cgDB+'].dbo.myWorkflowProcessModuleDefinition)
delete ['+@cgDB+'].dbo.myWorkflowCCModuleConditionDefinition where ProcessDefinitionGUID not in (SELECT ProcessDefinitionGUID FROM ['+@cgDB+'].dbo.myWorkflowProcessModuleDefinition)'

exec (@sql )

set @sql='insert into ['+@cgDB+'].dbo.myWorkflowProcessModuleDefinition  select * from ['+@hxDB+'].dbo.myWorkflowProcessModuleDefinition a where a.Application <> ''0101'' and not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowProcessModuleDefinition b where b.ProcessDefinitionGUID=a.ProcessDefinitionGUID)
insert into ['+@cgDB+'].dbo.myWorkflowStepModuleDefinition  select * FROM ['+@hxDB+'].dbo.myWorkflowStepModuleDefinition a where not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowStepModuleDefinition b where b.ProcessDefinitionGUID=a.ProcessDefinitionGUID)
insert into ['+@cgDB+'].dbo.myWorkflowBandModuleDefinition  select * FROM ['+@hxDB+'].dbo.myWorkflowBandModuleDefinition a where not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowBandModuleDefinition b where b.ProcessDefinitionGUID=a.ProcessDefinitionGUID)
insert into ['+@cgDB+'].dbo.myWorkflowCCModuleConditionDefinition select * FROM ['+@hxDB+'].dbo.myWorkflowCCModuleConditionDefinition a where not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowCCModuleConditionDefinition b where b.ProcessDefinitionGUID=a.ProcessDefinitionGUID)
insert into ['+@cgDB+'].dbo.myWorkflowCCModuleDefinition select * FROM ['+@hxDB+'].dbo.myWorkflowCCModuleDefinition a where not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowCCModuleDefinition b where b.ProcessDefinitionGUID=a.ProcessDefinitionGUID)
insert into ['+@cgDB+'].dbo.myWorkflowDocument select * FROM ['+@hxDB+'].dbo.myWorkflowDocument a where not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowDocument b where b.DocumentGUID=a.DocumentGUID)
insert into ['+@cgDB+'].dbo.myWorkflowBizType select * FROM ['+@hxDB+'].dbo.myWorkflowBizType a where not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowBizType b where b.BusinessTypeGUID=a.BusinessTypeGUID)
insert into ['+@cgDB+'].dbo.myWorkflowRelationModuleDefinition select * FROM ['+@hxDB+'].dbo.myWorkflowRelationModuleDefinition a where not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowRelationModuleDefinition b where b.RelationDefinitionGUID=a.RelationDefinitionGUID)'
exec(@sql)

set @sql='insert into ['+@cgDB+'].dbo.myWorkflowBusinessObject(BusinessObjectGUID,Name,Type,Description,MainTable,MainKey,BookType,BookStoredProcedure,EnabledExtendBusinessObjectXML,ExtendBusinessObjectXMLUrl,BusinessTypeGUID,BusinessJSFile,SelectPage,ApproveModifyURL,DataSourceType,DataSourceSPName,EventHandlerType,EventHandlerSPName,CreatedByName,CreatedOn,ModifiedByName,ModifiedOn,InitiateModifyURL,ServiceURL,SiteGUID,AfterHandleFinish) 
select BusinessObjectGUID,Name,Type,Description,MainTable,MainKey,BookType,BookStoredProcedure,EnabledExtendBusinessObjectXML,ExtendBusinessObjectXMLUrl,BusinessTypeGUID,BusinessJSFile,SelectPage,ApproveModifyURL,DataSourceType,DataSourceSPName,EventHandlerType,EventHandlerSPName,CreatedByName,CreatedOn,ModifiedByName,ModifiedOn,InitiateModifyURL,ServiceURL,SiteGUID,AfterHandleFinish FROM ['+@hxDB+'].dbo.myWorkflowBusinessObject a where BusinessTypeGUID not in (select BusinessTypeGUID from ['+@hxDB+'].dbo.myWorkflowBizType where Application = ''0101'') and not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowBusinessObject b where b.BusinessObjectGUID=a.BusinessObjectGUID)
insert into ['+@cgDB+'].dbo.myWorkflowBusinessLink  select * FROM ['+@hxDB+'].dbo.myWorkflowBusinessLink a where not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowBusinessLink b where b.BusinessLinkGUID=a.BusinessLinkGUID)
 insert into ['+@cgDB+'].dbo.myworkflowAttachments select * FROM ['+@hxDB+'].dbo.myworkflowAttachments a where not exists (select 1 from ['+@cgDB+'].dbo.myworkflowAttachments b where b.DocGUID=a.DocGUID)
insert into ['+@cgDB+'].dbo.myWorkflowDocDir select * FROM ['+@hxDB+'].dbo.myWorkflowDocDir a where not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowDocDir b where b.DirectoryGUID=a.DirectoryGUID)
insert into ['+@cgDB+'].dbo.myWorkflowProcessKind select * FROM ['+@hxDB+'].dbo.myWorkflowProcessKind a where name not in( ''业务系统流程模板'',''销售管理'') and not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowProcessKind b where b.ProcessKindGUID=a.ProcessKindGUID)
insert into ['+@cgDB+'].dbo.myWorkflowEntityDataSource select * FROM ['+@hxDB+'].dbo.myWorkflowEntityDataSource a where not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowEntityDataSource b where b.DataSourceGUID=a.DataSourceGUID)
insert into ['+@cgDB+'].dbo.myWorkflowEventHandlerClass select * FROM ['+@hxDB+'].dbo.myWorkflowEventHandlerClass a where not exists (select 1 from ['+@cgDB+'].dbo.myWorkflowEventHandlerClass b where b.EventHandlerClassGUID=a.EventHandlerClassGUID)
'
 
 exec (@sql);
 
--在重构数据库中执行，mip初始化之后执行
update myWorkflowApplication set siteGUID = s.siteGUID  from myworkflowSite s 
where application  in ('1001') 
and s.siteName ='工作流站点'

update myWorkflowApplication set siteGUID = s.siteGUID  from myworkflowSite s 
where application  in ('0000','0011','0001','0301','0302') 
and s.siteName ='重构ERP'
  
update myWorkflowApplication set siteGUID = s.siteGUID  from myworkflowSite s 
where application  in ('0201','0206','0202','0205','5001') 
and s.siteName ='核心ERP'

update myWorkflowApplication set siteGUID = s.siteGUID  from myworkflowSite s 
where application  in ('0220') 
and s.siteName ='采招系统'


update myWorkflowBusinessObject set siteguid = s.siteguid   
from myworkflowBizType b ,myWorkflowSite s,myWorkflowApplication app   
where 
app.siteGUID=s.siteGUID and b.Application=app.application and
myWorkflowBusinessObject.BusinessTypeGUID=b.BusinessTypeGUID 
 
update dbo.myWorkflowProcessKind set ParentGUID = 'B7C5A3FB-1A47-44DB-AB4F-8B3484DFA08C' where ParentGUID = '2A35334A-85C0-E311-8FB3-00155D0AB83E'
 