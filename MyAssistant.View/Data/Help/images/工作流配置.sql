DELETE dbo.myWorkflowSite WHERE siteName IN ('重构ERP','核心ERP','采招系统','工作流站点')
GO

DECLARE @yunERPSite VARCHAR(100)
DECLARE @wfSite VARCHAR(100)
DECLARE @ERPSite VARCHAR(100)
DECLARE @CZSite VARCHAR(100)
/*************************************执行前请先进行替换***********************************************/

SET @yunERPSite='http://重构地址'  --替换为重构的地址
SET @ERPSite='http://核心ERP地址'  --替换为核心ERP地址
SET @CZSite='http://采招地址'      --替换为采招地址
SET @wfSite='http://工作流地址'    --替换为工作流地址

/*************************************执行前请先进行替换**********************************************/
INSERT INTO dbo.myWorkflowSite
        ( siteGUID ,
          siteName ,
          sitePath ,
          wsSitePath ,
          isMainSite ,
          siteType
        )
VALUES  ( NEWID() , -- siteGUID - uniqueidentifier
          N'重构ERP' , -- siteName - nvarchar(40)
          @yunERPSite , -- sitePath - nvarchar(1024)
          N'' , -- wsSitePath - nvarchar(1024)
          1 , -- isMainSite - tinyint
          2  -- siteType - tinyint
        )


INSERT INTO dbo.myWorkflowSite
        ( siteGUID ,
          siteName ,
          sitePath ,
          wsSitePath ,
          isMainSite ,
          siteType
        )
VALUES  ( NEWID() , -- siteGUID - uniqueidentifier
          N'核心ERP' , -- siteName - nvarchar(40)
          @ERPSite , -- sitePath - nvarchar(1024)
          N'' , -- wsSitePath - nvarchar(1024)
          0 , -- isMainSite - tinyint
          1  -- siteType - tinyint
        )
INSERT INTO dbo.myWorkflowSite
        ( siteGUID ,
          siteName ,
          sitePath ,
          wsSitePath ,
          isMainSite ,
          siteType
        )
VALUES  ( NEWID() , -- siteGUID - uniqueidentifier
          N'采招系统' , -- siteName - nvarchar(40)
          @CZSite , -- sitePath - nvarchar(1024)
          N'' , -- wsSitePath - nvarchar(1024)
          0 , -- isMainSite - tinyint
          1  -- siteType - tinyint
        )

INSERT INTO dbo.myWorkflowSite
        ( siteGUID ,
          siteName ,
          sitePath ,
          wsSitePath ,
          isMainSite ,
          siteType
        )
VALUES  ( NEWID() , -- siteGUID - uniqueidentifier
          N'工作流站点' , -- siteName - nvarchar(40)
          @wfSite , -- sitePath - nvarchar(1024)
          N'' , -- wsSitePath - nvarchar(1024)
          0 , -- isMainSite - tinyint
          1  -- siteType - tinyint
        )
        
        