﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using FSLib.Network.Http;
using MyAssistant.Core;
using MyAssistant.Model;
using MyAssistant.Plugin;
using MyAssistant.Services;
using Newtonsoft.Json;
using Splat;

namespace MyAssistant.Common.Inspect.Plugin
{
    [PluginClass]
    public class MIPInspect : PluginBase
    {
        public enum DetectionStatusEnum
        {
            /// <summary>
            ///     Success
            /// </summary>
            Success = 1,

            /// <summary>
            ///     警告
            /// </summary>
            Warning = 2,

            /// <summary>
            ///     Fail
            /// </summary>
            Failure = 3,

            /// <summary>
            ///     发生异常
            /// </summary>
            Exception = 4
        }

        private List<InspectionItem> _dynamicItems;

        public MIPInspect(ILibraryService libraryService, IDirectoryIISService iisService)
            : base(libraryService, iisService)
        {
        }


        public override bool CheckConfig()
        {
            return true;
        }

        public override HttpStatus InitSite()
        {
            var client = new HttpClient();

            if (UploadDatasourceZip(client, Path.Combine(AppInfo.MIPInitPath, "DataSource.zip")))
                if (UploadSiteZip(client, Path.Combine(AppInfo.MIPInitPath, "SiteInfo.zip")))
                {
                    var result = base.InitSite();
                    if (result.Succes)
                    {
                        if (UploadAppZip(client, Path.Combine(AppInfo.MIPInitPath, "Application")))
                        {
                            return result;
                        }
                    }
                    return result;
                }

            return new HttpStatus {Message = "初始化MIP更新包失败！", Succes = false};
        }


        /// <summary>
        /// 上传应用包
        /// </summary>
        /// <param name="client"></param>
        /// <param name="package"></param>
        /// <returns></returns>
        private bool UploadAppZip(HttpClient client, string package)
        {

            var hasValue = client.Get<List<object>>($"{Context.Url}/api/Deployment/GetAppManageDeployInfo").Send();

            if (!hasValue.IsSuccess)
                return false;


            if (hasValue.Result.Count != 0)
                return true;


            if (!Directory.Exists(package))
                throw new FileNotFoundException($"{package}目录不存在，请检查工具。");


            var path = Path.Combine(AppInfo.MIPInitPath, "application");

            var allfiles = Directory.GetFiles(path);

            if (allfiles.Length == 0)
                throw new FileNotFoundException($"{path}目录不存在文件，请检查工具。");

            foreach (var filePath in allfiles)
            {
                var fileName = Path.GetFileNameWithoutExtension(filePath);

                HttpPostFile file = new HttpPostFile(fileName, filePath);
                IDictionary<string, object> files = new Dictionary<string, object>();
                files.Add("initzip", file);

                var context =
                    client.Post<List<AddApplicationResult>>($"{Context.Url}/api/Deployment/AddApplicationZip", files)
                        .Send();

                if (!context.IsSuccess)
                    return false;

                var result = context.Result[0];

                var rename = new ReNameApplication
                {
                    Name = result.AppName,
                    Version = result.Version,
                    NewName = fileName
                };

                var renameContext = client.Post<string>($"{Context.Url}/api/Deployment/ReName",
                    new List<ReNameApplication> {rename}, null, true).Send();

                if (!renameContext.IsFinished)
                    return false;

                var deploy = new DeployApp()
                {
                    AppName = rename.NewName,
                    AppVersion = rename.Version
                };

                var deployContext = client.Post<string>($"{Context.Url}/api/Deployment/DeployApp",
                   new List<DeployApp> { deploy }, null, true).Send();

                if (!deployContext.IsFinished)
                    return false;

            }

            return true;
        }

        private bool UploadSiteZip(HttpClient client, string package)
        {

            var hasValue = client.Get<string>($"{Context.Url}/api/BusinessSiteConfigure/GetSiteTree").Send();

            if (hasValue.IsSuccess)
            {
                if (hasValue.Result != "" && hasValue.Result != "\"\"")
                    return true;

                if (File.Exists(package))
                {
                    HttpPostFile file = new HttpPostFile(package);
                    IDictionary<string, object> files = new Dictionary<string, object>();
                    files.Add("SiteInfo", file);

                    var context = client.Post<string>($"{Context.Url}/api/BusinessSiteConfigure/Import", files).Send();
                    if (context.IsFinished)
                        return true;
                }
                else
                {
                    throw new FileNotFoundException($"{package}文件不存在，请检查工具。");
                }
            }
            return false;
        }



        private bool UploadDatasourceZip(HttpClient client, string package)
        {

            var hasValue = client.Post<DataSourceResult>($"{Context.Url}/api/DIDataSource/FindAll").Send();

            if (hasValue.IsSuccess)
            {
                if (hasValue.Result != null && hasValue.Result.Data.Count != 0)
                    return true;

              
                if (File.Exists(package))
                {
                    HttpPostFile file = new HttpPostFile(package);

                    IDictionary<string, object> files = new Dictionary<string, object>();
                    files.Add("DataSource", file);

                    var context = client.Post<string>($"{Context.Url}/api/DIDataSource/Import", files).Send();
                    if (context.IsFinished)
                        return true;

                    return false;
                }

                throw new FileNotFoundException($"{package}文件不存在，请检查工具。");
            }
            return false;

        }



        /// <summary>
        /// 获取检查项
        /// </summary>
        /// <returns></returns>
        public override List<InspectionItem> GetInspectionItems()
        {
            var items = new List<InspectionItem>
            {
                new InspectionItem {Id = "inspectConnection", Description = "检测站点连接是否正常"}
            };

            _dynamicItems = LoadDynamicInspectionItems();
            if (_dynamicItems != null)
                items.AddRange(_dynamicItems);

            return items;
        }

        /// <summary>
        ///     获取动态的验证项
        /// </summary>
        /// <returns></returns>
        private List<InspectionItem> LoadDynamicInspectionItems()
        {
            try
            {
              
                var client = new HttpClient();
                var context = client.Get<List<InspectionItem>>($"{Context.Url}/api/Detection/GetAllDetectingList").Send();
                if (context.IsSuccess)
                    return context.Result;
            }
            catch (Exception ex)
            {
                this.Log().FatalException(ex.Message, ex);
            }
            return null;
        }


        /// <summary>
        /// 验证是否可以连接成功
        /// </summary>
        /// <returns></returns>
        [InspectPluginMethod(Id = "inspectConnection")]
        public InspectionResult InspectConnection()
        {
            if (_dynamicItems == null)
                return new InspectionResult {Status = EnumStatus.Fail, Help = new[] {$"站点{Context.Url}无法连接！"}};
            return new InspectionResult {Status = EnumStatus.Success};
        }


        /// <summary>
        ///     获取动态检查结果
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public override InspectionResult GetDynamicInspectionResult(string id)
        {
            var client = new HttpClient();
            var context =
                client.Get<MIPResult>($"{Context.Url}/api/Detection/Detecting?detectionId={id}").Send();

            if (context.IsSuccess)
            {
                var rtn = context.Result;
                return new InspectionResult
                {
                    Help = rtn.Data,
                    Status = rtn.Status == DetectionStatusEnum.Success ? EnumStatus.Success : EnumStatus.Fail
                };
            }
            return null;
        }

        public class MIPResult
        {
            public string DetectionId { get; set; }
            public DetectionStatusEnum Status { get; set; }
            public string[] Data { get; set; }
        }

        public class AddApplicationResult
        {
            public string AppName { get; set; }
            public string Version { get; set; }
            public DateTime CrateDate { get; set; }
        }


        public class ReNameApplication
        {
            public string NewName { get; set; }
            public string Name { get; set; }
            public string Version { get; set; }
        }



        public class DeployApp
        {
            public string AppName { get; set; }
            public string AppVersion { get; set; }
        }


        public class DataSourceResult
        {
            [JsonProperty("data")]
            public  List<object> Data { get; set; }
        }
    }
}