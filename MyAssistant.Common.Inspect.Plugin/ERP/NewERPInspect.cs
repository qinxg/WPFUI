﻿using MyAssistant.Model;
using MyAssistant.Plugin;
using MyAssistant.Services;

namespace MyAssistant.Common.Inspect.Plugin.ERP
{
    /// <summary>
    /// 重构检查
    /// </summary>
    [PluginClass]
    public class NewERPInspect : ERPInspectBase
    {
        public NewERPInspect(ILibraryService libraryService, IDirectoryIISService iisService) : base(libraryService, iisService)
        {
        }

        public override bool CheckConfig()
        {
            return true;
        }

        public override InspectionItem GetUpFilesInspection()
        {
            return null;
        }

        protected override void SetVirualPath()
        {
        }

        protected override void SetConfig()
        {
        }

        public override InspectionItem GetWebConfigInspection()
        {
            return null;
        }
    }
}