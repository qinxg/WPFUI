﻿using System.Linq;
using MyAssistant.Model;
using MyAssistant.Plugin;
using MyAssistant.Services;

namespace MyAssistant.Common.Inspect.Plugin.ERP
{
    /// <summary>
    /// 工作流检查
    /// </summary>
    [PluginClass]
    public class WFInspect : ERPInspectBase
    {
        public WFInspect(ILibraryService libraryService, IDirectoryIISService iisService) : base(libraryService, iisService)
        {

        }


        [InspectPluginMethod(Id = "inspectWF", Order = 4)]
        public override InspectionResult InspectWF()
        {
            return CheckWFSiteInfo(SiteResult.WF, "WF");
        }


        /// <summary>
        /// 检查工作流站点信息
        /// </summary>
        /// <param name="nodeResult"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        protected InspectionResult CheckWFSiteInfo(InitDataResult nodeResult, string nodeName)
        {
            var result = CheckCommon(nodeResult, nodeName);
            if (result.Status != EnumStatus.Success)
                return result;

            var query = (from local in LocalInitData
                join site in nodeResult.Sites
                on local.Id equals site.Id
                where local.Url != site.Url
                select $"{local.Type}配置{local.Url}与站点配置{site.Url}不一致").ToArray();


            if (query.Length > 0)
            {
                result.Status = EnumStatus.Fail;
                result.Help = query;
            }
            return result;
        }
    }
}