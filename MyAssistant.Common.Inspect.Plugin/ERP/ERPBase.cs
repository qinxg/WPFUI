﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using FSLib.Network.Http;
using MyAssistant.Management;
using MyAssistant.Model;
using MyAssistant.Plugin;
using MyAssistant.Services;
using MyAssistant.Utility;
using Splat;

namespace MyAssistant.Common.Inspect.Plugin.ERP
{
    /// <summary>
    /// ERP检查基类
    /// </summary>
    [PluginClass]
    public abstract class ERPInspectBase : PluginBase
    {

        protected SiteInspectResult SiteResult { get; set; }

        protected List<InitData> LocalInitData { get; set; }

        protected ERPInspectBase(ILibraryService libraryService, IDirectoryIISService iisService)
            : base(libraryService, iisService)
        {
        }


        public override void BeforeInit()
        {
            if (Context.Library.Mode != EnumMode.SignleServer) return;

            SetVirualPath();
            SetConfig();
            CopyUpFiles();
        }


     


        /// <summary>
        /// 设置upfiles虚拟目录
        /// </summary>
        protected virtual void SetVirualPath()
        {
            var mainName = Context.MainSiteIISName;
            var currentName = Context.SiteIISName;

            if ((string.IsNullOrEmpty(mainName) == false) && (string.IsNullOrEmpty(currentName) == false))
                _iisService.InitVirtualDirectory(mainName, currentName);
        }

        /// <summary>
        ///     设置配置文件
        /// </summary>
        protected virtual void SetConfig()
        {
            var currentName = Context.SiteIISName;

            if (string.IsNullOrEmpty(currentName))
                return;

            var siteIIS = _iisService.GetALLSites()?.FirstOrDefault(p => p.SiteName == currentName);

            if (siteIIS == null)
                return;

            ConfigOperator.Write(siteIIS.SitePath);
        }


        /// <summary>
        /// 拷贝upfiles
        /// </summary>
        protected virtual void CopyUpFiles()
        {
            
        }


        #region  获取检查项

        public override List<InspectionItem> GetInspectionItems()
        {
            var items = new List<InspectionItem>
            {
                new InspectionItem {Id = "inspectConnection", Description = "检测站点连接是否正常"}
            };

            var upfilesInspection = GetUpFilesInspection();

            if (upfilesInspection != null)
                items.Add(upfilesInspection);



            var webConfigInspection = GetWebConfigInspection();

            if (webConfigInspection != null)
                items.Add(webConfigInspection);



            items.AddRange(new List<InspectionItem>
            {
                new InspectionItem {Id = "inspectSSO", Description = "检测单点登录配置"},
                new InspectionItem {Id = "inspectMIP", Description = "检测MIP集成配置"},
                new InspectionItem {Id = "inspectWF", Description = "检测工作流配置"}
            });

            return items;
        }


        public virtual InspectionItem GetWebConfigInspection()
        {
            return new InspectionItem { Id = "inspectWebconfig", Description = "检查站点Web.config设置" };
        }

        public virtual InspectionItem GetUpFilesInspection()
        {

            if (Context.Library.Mode != EnumMode.SignleServer)
                return null;

            if (string.IsNullOrEmpty(Context.SiteIISName))
                return null;

            return new InspectionItem { Id = "inspectUpfiles", Description = "检查站点Upfiles设置" };
        }

        #endregion



        #region 检查项

        public override void BeforeInspect()
        {
            try
            {
                var client = new HttpClient();
                var context =
                    client.Get<SiteInspectResult>($"{Context.Url}{Context.CurrentSite.InitInspectRelativePath}").Send();
                if (context.IsSuccess)
                {
                    var result = context.Result;
                    result.Address = Context.Url;
                    SiteResult = result;
                }
            }
            catch (Exception ex)
            {
                this.Log().FatalException(ex.Message, ex);
            }

            LocalInitData = GetInitData(Context.Library.Sites);
        }

        [InspectPluginMethod(Id = "inspectConnection", Order = 1)]
        public virtual InspectionResult InspectConnection()
        {
            var result = new InspectionResult { Status = EnumStatus.Fail };
            try
            {
                var client = new HttpClient();
                var context =
                    client.Get<string>($"{Context.Url}").Send();
                if (context.IsSuccess)
                {
                    result.Status= EnumStatus.Success;;
                }
            }
            catch (Exception ex)
            {
                this.Log().FatalException(ex.Message, ex);
            }

            return result;
        }



        [InspectPluginMethod(Id = "inspectWebconfig", Order = 2)]
        public virtual InspectionResult InspectWebconfig()
        {
            if (SiteResult == null)
            {

                var msg = string.IsNullOrEmpty(Context.Url)
                    ? "站点地址未配置"
                    : $"站点{Context.Url}{Context.CurrentSite.InitInspectRelativePath}无法连接。";

                return new InspectionResult
                {
                    Status = EnumStatus.Fail,
                    Help = new[] { msg }
                };
            }

            return new InspectionResult { Status = EnumStatus.Success };
        }


        [InspectPluginMethod(Id = "inspectSSO", Order = 3)]
        public virtual InspectionResult InspectSSO()
        {
            return CheckSiteInfo(SiteResult.SSO, "SSO");
        }


        [InspectPluginMethod(Id = "inspectMIP", Order = 4)]
        public virtual InspectionResult InspectMIP()
        {
            return CheckSiteInfo(SiteResult.MIP, "MIP");
        }

        [InspectPluginMethod(Id = "inspectWF", Order = 5)]
        public virtual InspectionResult InspectWF()
        {
            return CheckSiteInfo(SiteResult.WF, "WF");
        }

        [InspectPluginMethod(Id = "inspectUpfiles", Order = 6)]
        public virtual InspectionResult InspectUpfiles()
        {

            var currentPath = Context.SiteIISName;
            var mainPath = Context.MainSiteIISName;
            try
            {
                if (_iisService.IsExistsVirtualDirectory(mainPath, currentPath))
                    return new InspectionResult { Status = EnumStatus.Success };

                return new InspectionResult
                {
                    Status = EnumStatus.Fail,
                    Help = new[] { "站点Upfiles虚拟目录设置错误，请手动设置或重新进行初始化" }
                };
            }
            catch (Exception ex)
            {
                return new InspectionResult
                {
                    Status = EnumStatus.Fail,
                    Help = new[] { ex.Message }
                };
            }
        }


        /// <summary>
        ///     检查站点信息
        /// </summary>
        /// <param name="nodeResult"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        protected virtual InspectionResult CheckSiteInfo(InitDataResult nodeResult, string nodeName)
        {
            var result = CheckCommon(nodeResult, nodeName);
            if (result.Status != EnumStatus.Success)
                return result;

            var query = (from local in LocalInitData
                         join site in nodeResult.Sites
                         on local.Type equals site.Type
                         where local.Url != site.Url
                         select $"{local.Type}配置{local.Url}与站点配置{site.Url}不一致").ToArray();


            if (query.Length > 0)
            {
                result.Status = EnumStatus.Fail;
                result.Help = query;
            }
            return result;
        }


        /// <summary>
        ///     基础检查
        /// </summary>
        /// <param name="nodeResult"></param>
        /// <param name="nodeName"></param>
        /// <returns></returns>
        protected InspectionResult CheckCommon(InitDataResult nodeResult, string nodeName)
        {
            var result = new InspectionResult
            {
                Status = EnumStatus.Fail
            };


            if (nodeResult == null)
            {
                result.Help = new[] { $"{nodeName}节点返回null" };
                return result;
            }

            if (!nodeResult.Enable)
            {
                result.Help = new[] { $"{nodeName}未启用" };
                return result;
            }

            result.Status = EnumStatus.Success;
            return result;
        }

        #endregion
    }
}

