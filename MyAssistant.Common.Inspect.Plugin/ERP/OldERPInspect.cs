﻿using System.IO;
using System.Linq;
using MyAssistant.Plugin;
using MyAssistant.Services;
using MyAssistant.Utility;

namespace MyAssistant.Common.Inspect.Plugin.ERP
{
    /// <summary>
    /// 核心检查
    /// </summary>
    [PluginClass]
    public class OldERPInspect : ERPInspectBase
    {
        public OldERPInspect(ILibraryService libraryService, IDirectoryIISService iisService) : base(libraryService, iisService)
        {
            
        }

        /// <summary>
        /// 拷贝config
        /// </summary>
        protected  override void CopyUpFiles()
        {
            var currentName = Context.SiteIISName;

            if (!_iisService.ExistVirualPath(currentName, "UpFiles"))
            {
                var mainName = Context.MainSiteIISName;

                var currentPath = _iisService.GetALLSites()?.FirstOrDefault(p => p.SiteName == currentName)?.SitePath;
                var mainPath = _iisService.GetALLSites()?.FirstOrDefault(p => p.SiteName == mainName)?.SitePath;

                if (string.IsNullOrEmpty(mainPath) || string.IsNullOrEmpty(currentPath))
                    return;

                FileOperator.DirectoryCopy(Path.Combine(currentPath, "UpFiles"), Path.Combine(mainPath, "UpFiles"), true);
            }
        }
    }
}