﻿using MyAssistant.Plugin;
using MyAssistant.Services;

namespace MyAssistant.Common.Inspect.Plugin.ERP
{
    /// <summary>
    /// 采招检查
    /// </summary>
    [PluginClass]
    public class CZInspect : ERPInspectBase
    {
        public CZInspect(ILibraryService libraryService, IDirectoryIISService iisService) : base(libraryService, iisService)
        {

        }
    }
}